/*=========================================================|
 | 文件名:  XCDMA.c
 | 描述:    DMA数据传输控制
 | 版本:    V0.3
 | 日期:    2023/08/27
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail:  libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.DMA内设控制(串口SPI通讯等);
 |  2.DMA处理"内设->内存"和"内存->内设"相互独立;
 |  3.仅支持Byte传输;
 |  4.传输时对应传输硬件需要做好错误中断处理(如串口错误溢出);
 +--- 实现说明
 |  DMA传输库实现原理是,将DMA传输作为一个环形队列的读写(出入)部分:
 |      DMA发送:DMA作为出队列,每次传输完成时判断队列中还有多少数据,在更新地址和数据长度,再度开启DMA;
 |      DMA接收:DMA作为入队列,每次传输完成时判断队列中还有多少空间,在更新地址和数据长度,再度开启DMA;
 |      注意: 要是到缓存尾部,传输需要分2次传输,才能完成循环;
 |  当队列写满数据的时候,写偏移会等于读偏移,这个时候和队列空的状态是一样,
 |  为了区分2个状态,我们约定队列写数据最后1字节不存在数据,这样得到:
 |      队列空: ReadOffset == WriteOffset
 |      队列满: (WriteOffset + 1) % BufSize == ReadOffset
 |      队列数据个数: (WriteOffset + BufSize - ReadOffset) % BufSize
 |      队列有效长度: BufSize -1
 |  DMA传输只能得到剩余传输数量,所以需要保存当前传输的数量:
 |      RWSize: 读写长度(当前传输数量);
 |  所以得到已经传输的数据数量是:
 |      当前已经传输的数量: RWSize - XCDMA_Weak_GetCurrDataCounter(ph)
 +-----------------------------------------------|
 +--- 版本说明
 |  V1.01:-2022/12/14
 |      1.初始化
 |  V0.02:-2023/01/04
 |      1.修改DMA传输队列判断,用缓存最后一字节判断队列是否空满;
 |      2.修正DMA接收数据时,偶尔返回队列全满的bug(读数据长度时被DMA传输中断所引起);
 |      3.优化数据传输队列(解决当缓存大小在65535时,出入队列偏移大于16位数据时的错误);
 |  V0.03:-2023/08/27
 |      1.增加"内存"->"片内外设"传输是的传输开始和传输完成的弱函数:
 |          "XCDMA_Weak_TxDMATS"传输开始;
 |          "XCDMA_Weak_TxDMATC"传输完成;
 *========================================================*/
//=== 头文件
    #include "XCDMA.h"

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 私有类型 ===================================|
//=== 私有宏定义 =================================|
//=== 私有宏定义配置 =============================|
//=== 私有函数宏 =================================|
//=== 私有全局变量 ===============================|
//=== 私有函数 ===================================|
    //弱函数
    extern ushort XCDMA_Weak_GetCurrDataCounter(HandleXCDMA *ph);                   //获取当前DMA传输剩余数据计数器
    extern void XCDMA_Weak_StartDMA(HandleXCDMA *ph, uchar *pAddr, ushort Len);     //设置DMA接收地址和传输大小,并启动
    extern void XCDMA_Weak_TxDMATS(HandleXCDMA *ph);                                //传输开始
    extern void XCDMA_Weak_TxDMATC(HandleXCDMA *ph);                                //传输完成
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************|
 * 描述:    初始化DMA
 * 函数名:  XCDMA_Init
 * 参数[H]: HandleXCDMA *ph     //句柄
 * 参数[I]: uchar ID            //句柄ID
 * 参数[I]: TypeXCDMA_Type Type //传输类型(_XCDMA_Type_TXD:发送;_XCDMA_Type_RXD:接收)
 * 参数[I]: uchar *pBufAddr     //传输缓存地址
 * 参数[I]: ushort BufSize      //传输缓存大小
 * 返回:    void
 *** 说明:
 ***    初始化配置
 ***    ID不参与计算,只是在弱函数中方便用户处理数据(区分发送接收等),
 ***    若是不用可以全部写0;
 *** 例程:  无
 ************************************************/
void XCDMA_Init(HandleXCDMA *ph, uchar ID, TypeXCDMA_Type Type, uchar *pBufAddr, ushort BufSize)
{
    //设置缓存
    ph->pBufAddr = pBufAddr;
    ph->BufSize  = BufSize;
    //偏移数据处理
    ph->ReadOffset  = 0;    //读偏移
    ph->WriteOffset = 0;    //写偏移
    ph->RWSize      = 0;    //读写长度
    //其他数据处理
    ph->Error = _XCDMA_R_OK;    //错误状态(值是:TypeXCDMA_Return)
    ph->ID    = ID;             //用于区分DMA的ID(ID不参与计算,只是在弱函数中方便用户处理数据,若是不用可以全部写0)
    ph->Type  = Type;           //传输类型(值是:TypeXCDMA_Type)
    ph->Lock  = 0;              //清锁
}

/************************************************ 我是分割线 ************************************************/

/************************************************|
 * 描述:    获取数据长度
 * 函数名:  XCDMA_GetDataLen
 * 参数[H]: HandleXCDMA *ph
 * 返回:    ushort      //返回有效数据长度
 *** 说明:  获取有效数据长度
 *** 例程:  无
 ************************************************/
ushort XCDMA_GetDataLen(HandleXCDMA *ph)
{
    ushort Len;
    ulong ReadOffset;
    ulong WriteOffset;
    ushort TransferLen;
//===
    TransferLen = ph->RWSize - XCDMA_Weak_GetCurrDataCounter(ph);   //得到已经传输长度

    //发送数据
    if(ph->Type == _XCDMA_Type_TXD){
        /**得到读位置,
         * 单次传输不会超过连续缓存最大地址,所有不用"%BufSize"来防止溢出;
         */
        ReadOffset  = ph->ReadOffset + TransferLen;
        WriteOffset = ph->WriteOffset;
    }
    //接收数据
    else{
        /**得到写位置,
         * 单次传输不会超过连续缓存最大地址,所有不用"%BufSize"来防止溢出;
         */
        WriteOffset = ph->WriteOffset + TransferLen;
        ReadOffset  = ph->ReadOffset;
    }
    //计算长度((WriteOffset + BufSize - ReadOffset) % BufSize)
    WriteOffset += ph->BufSize;
    WriteOffset -= ReadOffset;
    Len = WriteOffset % ph->BufSize;   //数据长度

    return(Len);
}

/************************************************|
 * 描述:    获取空闲空间长度
 * 函数名:  XCDMA_GetFreeLen
 * 参数[H]: HandleXCDMA *ph
 * 返回:    ushort      //得到空闲空间值
 *** 说明: 获取空闲空间长度
 ***    队列最后一字节是用来判断空或者满的,所以数据要"-1"
 *** 例程: 无
 ************************************************/
ushort XCDMA_GetFreeLen(HandleXCDMA *ph)
{
    return( (ph->BufSize-1)  - XCDMA_GetDataLen(ph) );
}

/************************************************|
 * 描述:    获取错误
 * 函数名:  XCDMA_GetError
 * 参数[H]: HandleXCDMA *ph
 * 返回:    int
 *  +=状态
 *  |   _XCDMA_R_OK:      正常;
 *  |   _XCDMA_R_NoSpace: 写数据缓存满,写数据暂停,需要重启DMA;
 *** 说明: 用于DMA接收数据中:
 ***    在DMA传输完成中断中判断,若是没有空闲空间则错误,并会停止DMA传输;
 ***    所以在接收DMA传输的时候一定要做错误判断处理;
 ***    注:没有空间停止DMA时,对应接口的硬件会带一些错误码,用户要记得去清除;
 ***        比如串口的溢出错误,需要手动清除,不然将不会继续接收数据;
 ***    重启接收传输会清除错误标志;
 *** 例程:  无
 ************************************************/
int XCDMA_GetError(HandleXCDMA *ph)
{
    return(ph->Error);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/*=== DMA发送数据(内存->内设)
 *  DMA说明:
 *  1.DMA和对应的内设必须都配置完成;
 *  2.DMA是单次传输模式,不能是循环模式;
 *  3.DMA需要配置完成传输中断;
 *  4.DMA初始化设定的时候传输数量必须填"0";
 */

/************************************************|
 * 描述:    [私有]DMA更新发送参数
 * 函数名:  XCDMA_TxUpdateSendParam
 * 参数[I]: HandleXCDMA *ph //句柄
 * 参数[I]: ushort SendLen  //发送长度
 * 返回:    void
 *** 说明:
 ***    调用此函数后可以发送数据,
 ***    发送数据的地址是:ph->pBufAddr+ph->ReadOffset
 ***    发送数据的长度是:ph->RWSize
 ***    要调用发送函数为:
 ***    XCDMA_Weak_StartDMA(ph, ph->pBufAddr+ph->ReadOffset, ph->RWSize);
 *** 例程:  无
 ************************************************/
static void XCDMA_TxUpdateSendParam(HandleXCDMA *ph, ushort SendLen)
{
    ushort EndLen;
    ulong ReadOffset;
//===
    //得到最新读偏移(上次传输完成的读偏移)
    ReadOffset  = ph->ReadOffset;
    ReadOffset += ph->RWSize;                       //指向最新地址(RWSize一定<=BufSize)
    ph->ReadOffset = ReadOffset % ph->BufSize;  //得到偏移
    //得到传输长度
    EndLen = ph->BufSize - ph->ReadOffset;          //到缓存尾部的长度
    ph->RWSize = SendLen>EndLen ? EndLen : SendLen; //传输数量大于最大缓存,按到尾部算
}

/************************************************|
 * 描述:    发送数据
 * 函数名:  XCDMA_TxSendData
 * 参数[I]: HandleXCDMA *ph
 * 参数[I]: uchar *pAddr    //发送缓存地址
 * 参数[I]: ushort Len      //发送长度
 * 返回:    int
 *  +=状态
 *  |   _XCDMA_R_OK
 *  |   _XCDMA_R_NoSpace    //缓存空间不足(数据不会传输)
 *** 说明:  用DMA发送数据
 *** 例程:  无
 ************************************************/
int XCDMA_TxSendData(HandleXCDMA *ph, uchar *pAddr, ushort Len)
{
    ushort FreeSize;
    ushort SendLen;
    ulong WriteOffset;
//===
    FreeSize = XCDMA_GetFreeLen(ph);            //得到空闲空间
    if(Len > FreeSize){
        return(_XCDMA_R_NoSpace);               //=空间不足
    }
    SendLen = Len;                              //发送长度
//=== 入队列
    WriteOffset = ph->WriteOffset;
    while(Len--){
        *(ph->pBufAddr + WriteOffset++) = *pAddr++;
        WriteOffset %= ph->BufSize;
    }
    ph->WriteOffset = WriteOffset;
//=== DMA操作
    if(XCDMA_Weak_GetCurrDataCounter(ph) == 0){
        //已经完成传输,需要重新启动传输;
        XCDMA_TxUpdateSendParam(ph, SendLen);   //更新发送参数
        XCDMA_Weak_TxDMATS(ph);                 //启动传输前调用
        //传输数据
        XCDMA_Weak_StartDMA(ph, ph->pBufAddr+ph->ReadOffset, ph->RWSize);
    }
//===
    return(_XCDMA_R_OK);
}

/************************************************|
 * 描述:    发送数据DMA中断服务
 * 函数名:  XCDMA_TxISR
 * 参数[I]: HandleXCDMA *ph
 * 返回:    void
 *** 说明:  放在中断处理中;
 *** 例程:  无
 ************************************************/
void XCDMA_TxISR(HandleXCDMA *ph)
{
    ushort SendLen;
//===
    SendLen = XCDMA_GetDataLen(ph);
    //有数据则重启传输
    if(SendLen){
        XCDMA_TxUpdateSendParam(ph, SendLen);   //更新发送参数
        XCDMA_Weak_StartDMA(ph, ph->pBufAddr+ph->ReadOffset, ph->RWSize);
    }
    else{
        //没有数据调用传输结束;
        XCDMA_Weak_TxDMATC(ph);
    }
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/*=== DMA接收数据(内设->内存)
 *  DMA说明:
 *  0.接收数据本质处理是将接收数据的操作作为环形队列的入队列;
 *  1.接收缓存地址就是DMA数据传输的地址,大小也必须一样;
 *  2.接收DMA必须开接收完成中断,并调用库中ISR,注意DMA不要开循环传输;
 *  3.接收DMA必须时刻判断DMA错误状态,以防止队列满出错(中断处理也会返回,可以做消息通知)
 *  4.配置好硬件后由"XCDMA_RxStartDMA"启动传输,错误后也由此函数重启传输;
 *  5.注意,对应硬件的接口的错误中断需要自行处理,如串口接收溢出错误;
 */

/************************************************|
 * 描述:    从缓存中获取数据
 * 函数名:  XCDMA_RxGetData
 * 参数[H]: HandleXCDMA *ph
 * 参数[I]: uchar *pAddr    //接收数据的地址(NULL表示抛弃)
 * 参数[I]: ushort Len      //需要获取接收数据的大小
 * 返回:    ushort          //实际输出数据大小
 *** 说明:
 ***    在DMA写数据到队列,在从队列中获取数据;
 ***    数据有多少出多少;
 ***    此函数是一个出队列的过程;
 *** 例程:  无
 ************************************************/
ushort XCDMA_RxGetData(HandleXCDMA *ph, uchar *pAddr, ushort Len)
{
    ushort DataLen;
    ulong ReadOffset;
//===
    /**这里do{}while是为了解决在读取XCDMA_GetDataLen时DMA中断导致部分全局数据被中断改变,
     *  从而获取的"数据长度"是错误的;
     *  这里增加了一个锁,要是读XCDMA_GetDataLen时被中断,则重新读取一次;
     */
    do{
        ph->Lock = 0;
        DataLen = XCDMA_GetDataLen(ph);     //获取有效数据长度
    }while(ph->Lock);

    if(DataLen == 0){ return(0); }      //=无数据直接跳出
    if(Len > DataLen){
        Len = DataLen;
    }
    DataLen = Len;                      //返回用
//=== 数据处理
    if(pAddr == NULL){
        //抛弃数据
        ReadOffset  = ph->ReadOffset;
        ReadOffset += Len;
        ph->ReadOffset = ReadOffset % ph->BufSize;
    }
    else{
        //出队列
        ReadOffset = ph->ReadOffset;
        while(Len--){
            *pAddr++ = *(ph->pBufAddr + ReadOffset++);
            ReadOffset %= ph->BufSize;
        }
        ph->ReadOffset = ReadOffset;
    }
//===
    return(DataLen);
}

/************************************************|
 * 描述:    DMA启动接收传输
 * 函数名:  XCDMA_RxStartDMA
 * 参数[H]: HandleXCDMA *ph
 * 返回:    int
 *  +=状态
 *  |   _XCDMA_R_OK: ok
 *  |   _XCDMA_R_NoSpace    //队列空间不足,DMA传输中断
 *** 说明:  用于启动接收DMA传输;
 ***    DMA接收处理,初始时必须调用此函数才开始接收;
 ***    当DMA缓存不足错误的时候("XCDMA_GetError"返回"_XCDMA_R_NoSpace"),
 ***    必须调用此函数重启接收DMA;
 *** 例程:  无
 ************************************************/
int XCDMA_RxStartDMA(HandleXCDMA *ph)
{
    ushort EndLen;
    ushort FreeLen;     //空闲长度
    ulong WriteOffset;
//=== 判断队列是否满了
    FreeLen = XCDMA_GetFreeLen(ph);     //获取空闲空间长度
    if(FreeLen == 0){
        ph->Error = _XCDMA_R_NoSpace;   //队列空间不足
        return(_XCDMA_R_NoSpace);
    }
    else{
        ph->Error = _XCDMA_R_OK;        //清错误状态
    }
//=== 下个传输配置
    //得到最新写偏移
    WriteOffset  = ph->WriteOffset;
    WriteOffset += ph->RWSize;
    ph->WriteOffset = WriteOffset % ph->BufSize;
    //得到传输长度
    EndLen = ph->BufSize - ph->WriteOffset;         //到缓存尾部的长度
    ph->RWSize = FreeLen>EndLen ? EndLen : FreeLen; //传输数量大于最大缓存,按到尾部算
    //传输
    XCDMA_Weak_StartDMA(ph, ph->pBufAddr+ph->WriteOffset, ph->RWSize);  //开始下次传输
//===
    return(_XCDMA_R_OK);
}

/************************************************|
 * 描述:    接收数据DMA中断服务
 * 函数名:  XCDMA_RxISR
 * 参数[H]: HandleXCDMA_Rx *ph
 * 返回:    int
 *  +=状态
 *  |   _XCDMA_R_OK
 *  |   _XCDMA_R_NoSpace    //队列空间不足,DMA传输中断
 *** 说明:  放在中断处理中;
 *** 例程:  无
 ************************************************/
int XCDMA_RxISR(HandleXCDMA *ph)
{
    ph->Lock = 1;       //上锁
    //开启下次传输
    return(XCDMA_RxStartDMA(ph));
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
#if(_XCDMA_WeakFunc_EN == 1)
/*=== 弱函数实现-说明*/
/**发送和接收数据的若函数通用,可以用句柄地址或者ID来区分调用;
 */

/************************************************|
 * 描述:    [弱函数]-获取当前DMA传输剩余数据计数器
 * 函数名:  XCDMA_Weak_GetCurrDataCounter
 * 参数[H]: HandleXCDMA *ph
 * 返回:    ushort      //得到计数值
 *** 说明:  获取传输剩余字节数,每传输一个当前计数减1;
 *** 例程: 无
 ************************************************/
__WEAK ushort XCDMA_Weak_GetCurrDataCounter(HandleXCDMA *ph)
{
    return(0);
}

/************************************************|
 * 描述:    [弱函数]-设置DMA接收地址和传输大小,并启动
 * 函数名:  XCDMA_Weak_StartDMA
 * 参数[H]: HandleXCDMA *ph
 * 参数[I]: uchar *pAddr    //接收缓存地址
 * 参数[I]: ushort Len      //接收缓存
 * 返回:    void
 *** 说明: 启动DMA传输;
 *** 例程: 无
 ************************************************/
__WEAK void XCDMA_Weak_StartDMA(HandleXCDMA *ph, uchar *pAddr, ushort Len)
{}


//=== 下面2个弱函数是选配(一般不用到)

/************************************************|
 * 描述:    [弱函数]-传输开始
 * 函数名:  XCDMA_Weak_DMATS
 * 参数[H]: HandleXCDMA *ph
 * 返回:    void
 *** 说明:
 ***   *只在"内存"->"片内外设"传输时有效;
 ***    当首次或上次DMA传输结束时,再次启动传输会调用此函数;
 ***    可用作RS485发送接收转换;
 ***    注意:要是传输数据量大且一直传输,可能只有在首次启动DMA时会调用;
 ***    在DMA发送时:
 ***        发送数据的地址是:ph->pBufAddr+ph->ReadOffset
 ***        发送数据的长度是:ph->RWSize
 *** 例程: 无
 ************************************************/
__WEAK void XCDMA_Weak_TxDMATS(HandleXCDMA *ph)
{}

/************************************************|
 * 描述:    [弱函数]-传输完成
 * 函数名:  XCDMA_Weak_DMATC
 * 参数[H]: HandleXCDMA *ph
 * 返回:    void
 *** 说明:
 ***   *只在"内存"->"片内外设"传输时有效;
 ***    当DMA传输完成,且没有新数据的时候会调用;
 ***    注意:这个调用在DMA中断中;
 ***    注意:要是传输数据量大且一直传输,次函数可能一直不会被调用;
 ***    可用作RS485发送接收转换;
 *** 例程: 无
 ************************************************/
__WEAK void XCDMA_Weak_TxDMATC(HandleXCDMA *ph)
{}

//===
#endif

/************************************************ 我是分割线 ************************************************/
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
