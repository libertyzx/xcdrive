/*=========================================================|
 | 文件名:  BM2166.h
 | 描述:    圆形半导体指纹头-函数声明
 | 版本:    V0.01
 | 日期:    2020/10/09
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail:  libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.圆形半导体指纹头(带七彩光)
 +-----------------------------------------------|
 +--- 版本说明
 |  V0.01:-2020/10/09
 |      1.见".c"文件
 *========================================================*/
//=== 防重复定义
#ifndef _BM2166_H_
#define _BM2166_H_
//=== 头文件
    #include "XCBase.h"
    #include "CBFIFO.h"     //队列

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 数据配置宏 ==========================================|
    //-是否开启弱函数(1:开;0:关)
    #ifndef _BM2166_WeakFunc_EN
        #define _BM2166_WeakFunc_EN     (1)
    #endif

    //指纹注册配置
    #define _BM2166_Cnd_AutoEnroll_InNum    /*录入次数*/                                    (4)
    #define _BM2166_Cnd_AutoEnroll_LED      /*[B0];0常亮,1获取图像后熄灭*/                  (1)
    #define _BM2166_Cnd_AutoEnroll_Pre      /*[B1];预处理:0关闭,1开启*/                     (1)
    #define _BM2166_Cnd_AutoEnroll_Ret      /*[B2];步骤返回:0返回,1不返回*/                 (0)
    #define _BM2166_Cnd_AutoEnroll_Cover    /*[B3];覆盖ID:0不允许,1允许*/                   (0)
    #define _BM2166_Cnd_AutoEnroll_ReReg    /*[B4];重复注册:0允许,1不允许*/                 (1)
    #define _BM2166_Cnd_AutoEnroll_Leave    /*[B5];多次注册是否要手指离开;0离开,1不离开*/   (0)
    #define _BM2166_Cnd_AutoEnroll_Same     /*[B6];不同手指录入:0允许,1不允许*/             (1)

    //其他
    #define _BM2166_BSem_Num        /*开关信号量数量*/  (8)

/************************************************ 我是分割线 ************************************************/
//=== 宏定义|枚举变量 =====================================|
    //返回
    #define _BM2166_R_GoOn                  /*继续*/           (1)
    #define _BM2166_R_OK                    /*OK*/             (0)
    #define _BM2166_R_Fail                  /*失败*/           (-1)
    #define _BM2166_R_CmdNotExist           /*指令不存在*/     (-2)

    //其他参数
    #define _BM2166_P_NullID                /*空ID*/           (0xFFFF)

    //模块内部的确认码
    typedef enum{
        _BM2166_CC_Succeed = 0x00,          /*成功*/
        _BM2166_CC_Fail    = 0x01,          /*失败*/
        _BM2166_CC_Timeout = 0x26,          /*超时*/
    }TypeBM2166_CC;

    //添加指纹时给回调的状态
    typedef enum{
        _BM2166_AddState_Start  = 0,        //开始
        _BM2166_AddState_Finish = -1,       //完成
        _BM2166_AddState_Error  = -2,       //错误
        /*大于0的是当前次数*/
    }TypeBM2166_AddState;

    //删除指纹时给回调的数据
    typedef enum{
        _BM2166_DelType_All      = 0,       //删除全部完成
        _BM2166_DelType_AllError = -1,      //删除所有错误
        _BM2166_DelType_Error    = -2,      //删除单个错误
        /*大于0的是单次删除的ID*/
    }TypeBM2166_DelType;

/************************************************ 我是分割线 ************************************************/
//=== 数据类型 ============================================|
/* 指纹句柄
 */ typedef volatile struct _HandleBM2166{
        //协程相关
        HandleXCCOR hCOR;           //协程句柄
        ulong CORTime;              //协程时间

        //芯片相关
        ulong ICAddr;               //芯片地址

        //通用数据
        uchar Buf[4];               //缓存
        uchar OrderCnf;             //序列控制
        uchar OrderState;           //顺序状态
        uchar CurrentCmd;           //当前处理的指令

        //参数|返回
        uchar  Param_CCode;         //确认码
        ushort Param_ID;            //添加删除的ID

        //发送相关
        uchar Tx_State;             //发送状态
        uchar Tx_RepeatNum;         //重复发送次数
        uchar *pTx_Param;           //发送用的参数([长度+指令+数据]*n)
        ushort Tx_ParamLen;         //发送用参数长度(总长度)
        _Mark_new(Tx_BSem, 8);      //开关信号量

        //接收相关
        HandleCBFIFO *phCBQ_Rx;     //接收_环形队列
        ulong Rx_Timeout;           //接收_超时
        ulong Rx_NextLen;           //接收_下个数据长度
        uchar Rx_FSMState;          //接收_状态机状态
        uchar Rx_PackMark;          //接收_包标识
        ushort Rx_PackLen;          //接收_包长度(数据+校验)

        //回调
        void(*fVerify)(volatile struct _HandleBM2166*, int);        //验证回调
        void(*fReg)(volatile struct _HandleBM2166*, int);           //注册回调
        void(*fDel)(volatile struct _HandleBM2166*, int);           //注册回调
    }HandleBM2166;

/* 指纹校验回调函数指针类型
 * 形参顺序说明:
 *  HandleBM2166 *ph    //句柄
 *  int Score           //得分
 */ typedef void(*TypeBM2166_fCB_Verify)(HandleBM2166*, int);
/* 指纹注册回调函数指针类型
 * 形参顺序说明:
 *  HandleBM2166 *ph    //句柄
 *  int AddState        //添加状态"TypeBM2166_AddState"
 */ typedef void(*TypeBM2166_fCB_Reg)(HandleBM2166*, int);
/* 指纹删除回调函数指针类型
 * 形参顺序说明:
 *  HandleBM2166 *ph    //句柄
 *  int Type            //删除类型"TypeBM2166_DelType"
 */ typedef void(*TypeBM2166_fCB_Del)(HandleBM2166*, int);

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 函数声明 ============================================|
/*=== 基础函数*/
    void BM2166_Init(HandleBM2166 *ph, ulong ICAddr, HandleCBFIFO *phCBQ, TypeBM2166_fCB_Verify fVerify, TypeBM2166_fCB_Reg fReg, TypeBM2166_fCB_Del fDel);
    int BM2166_Loop(HandleBM2166 *ph);

/*=== 设置*/
    void BM2166_SetRxCBFIFOHandle(HandleBM2166 *ph, HandleCBFIFO *phCBQ);           //设置环形队列句柄
    void BM2166_SetVerifyCallback(HandleBM2166 *ph, TypeBM2166_fCB_Verify fVerify); //设置指纹回调

/*=== 操作*/
    void BM2166_PowerUp(HandleBM2166 *ph);          //上电
    void BM2166_Add(HandleBM2166 *ph, ushort ID);   //添加指纹
    void BM2166_Verify(HandleBM2166 *ph);           //验证指纹
    void BM2166_Del(HandleBM2166 *ph, ushort ID);   //删除指纹
    void BM2166_DelAll(HandleBM2166 *ph);           //删除所有


//=== 函数宏 ==============================================|
/*=== 函数宏类型*/

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 文件结束
#endif
