/*=========================================================|
 | 文件名:  BM2166.c
 | 描述:    圆形半导体指纹头(带七彩光)
 | 版本:    V0.01
 | 日期:    2020/10/09
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail: libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.圆形半导体指纹(带七彩光)
 |  2.通讯口参数:
 |      半双工,波特率:57600bps;
 |      帧格式10位,1起始,8数据(低位前),2停止,无校验;
 |  3.
 +-----------------------------------------------|
 +--- 版本说明
 |  V0.01:-2020/10/09
 |      1.初始化
 *========================================================*/
//=== 头文件
    #include "BM2166.h"
    #include "Debug.h"
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 私有类型 ============================================|

//=== 私有数据配置宏 ======================================|
    //配置
    #define _BM22166_Cnf_RxBufLen   /*接收缓存大小(字节)*/  (160)
    #define _BM22166_Cnf_SendBufLen /*发送缓存大小(字节)*/  (160)
    //时间参数
    #define _RxTimeoutCount         /*接收超时计数*/        (99999)
//=== 私有宏定义 ==========================================|
    //长度
    #define _Len_PackHeadLen        /*包头长度*/        (2)            //包头(2B)
    #define _Len_InfoLen            /*信息长度*/        (4+1+2)        //芯片地址(4B)+包标识(1B)+包长度(2B)
    //包数据
    #define _PackHead0              /*包头0*/           (0xEF)
    #define _PackHead1              /*包头1*/           (0x01)
    //包标识
    #define _PackMark_Cmd           /*指令包*/          (0x01)
    #define _PackMark_Data          /*数据包*/          (0x02)
    #define _PackMark_ACK           /*反馈包*/          (0x07)
    #define _PackMark_EndData       /*最后一个数据包*/  (0x08)
    //其他数据
    #define _RxSyncFrame            /*接收同步帧*/      (0x55)
    //接收状态机状态
    #define _RxFSM_P_00             (0)
    #define _RxFSM_P_01             (1)
    #define _RxFSM_P_02             (2)

//=== 发送数据相关
    //发送数据状态
    #define _Tx_S_Init                  /*初始化*/      (0)
    #define _Tx_S_Send                  /*发送*/        (1)
    #define _Tx_S_WaitACK               /*等待反馈*/    (2)
    //发送开关信号量(8位)
    #define _TxBSem_ACK                 /*确认*/        (0)
    #define _TxBSem_RxSyncFrame         /*接收同步帧*/  (1)
    #define _TxBSem_Error               /*错误*/        (2)

//=== 顺序流程控制值
    //掩码值(8位)
    #define _OrderCnf_Mask_WakeUp       /*唤醒*/        (_B0000_0001)
    #define _OrderCnf_Mask_PowerUp      /*上电*/        _OrderCnf_Mask_WakeUp
    #define _OrderCnf_Mask_CloseVDD     /*关闭电源*/    (_B0000_0010)
    #define _OrderCnf_Mask_Handshake    /*握手指令*/    (_B0000_0100)
    #define _OrderCnf_Mask_Verify       /*校验*/        (_B0000_1000)
    #define _OrderCnf_Mask_Add          /*添加*/        (_B0001_0000)
    #define _OrderCnf_Mask_Del          /*删除*/        (_B0010_0000)
    #define _OrderCnf_Mask_DelAll       /*删除所有*/    (_B0100_0000)
    //状态
    #define _Order_S_NULL           /*初始,空*/      (0)
    #define _Order_S_Run            /*运行*/         (1)

//=== 指令
    //自动化指令(完整)
    #define _BM2166_Cmd_AutoEnroll          /*自动注册模板*/        (0x31)
    #define _BM2166_Cmd_AutoIdentify        /*自动验证指纹*/        (0x32)
    #define _BM2166_Cmd_DeletChar           /*删除模板*/            (0x0C)
    #define _BM2166_Cmd_Empty               /*清空指纹库*/          (0x0D)
    #define _BM2166_Cmd_Cancel              /*取消指令*/            (0x30)
    #define _BM2166_Cmd_Sleep               /*休眠指令*/            (0x33)
    #define _BM2166_Cmd_ValidTempleteNum    /*读有效模板个数*/      (0x1D)
    #define _BM2166_Cmd_SetPwd              /*设置口令*/            (0x12)
    #define _BM2166_Cmd_VfyPwd              /*验证口令*/            (0x13)
    #define _BM2166_Cmd_AuraLedConfig       /*光环控制*/            (0x3C)
    //指令(部分)
    #define _BM2166_Cmd_ReadSysPara         /*读系统基本参数*/      (0x0F)
    #define _BM2166_Cmd_ReadIndexTable      /*读索引表*/            (0x1F)
    #define _BM2166_Cmd_GetSN               /*获取芯片唯一序列号*/  (0x34)
    #define _BM2166_Cmd_Handshake           /*握手指令*/            (0x35)
    #define _BM2166_Cmd_ValidationSensor    /*校验传感器*/          (0x36)

//=== 自动注册指令反馈的状态码
    //注册反馈状态码
    #define _AutoEnroll_State_Validity      /*指纹合法性检查*/          (0x00)
    #define _AutoEnroll_State_GetImage      /*获取图像*/                (0x01)
    #define _AutoEnroll_State_GenChar       /*生成特征值*/              (0x02)
    #define _AutoEnroll_State_Leave         /*判断手指是否离开*/        (0x03)
    #define _AutoEnroll_State_RegModel      /*合并模板*/                (0x04)
    #define _AutoEnroll_State_RegVerify     /*注册检验*/                (0x05)
    #define _AutoEnroll_State_Save          /*存储模板*/                (0x06)
    //注册反馈码
    #define _AutoEnroll_FBack_Validity      /*指纹合法性检测*/          (0x00)
    #define _AutoEnroll_FBack_RegModel      /*合并模板*/                (0xF0)
    #define _AutoEnroll_FBack_Exist         /*检验该手指是否已注册*/    (0xF1)
    #define _AutoEnroll_FBack_Save          /*存储模板*/                (0XF2)

//=== 私有函数宏定义 ======================================|
//=== 只在"Order"中调用
    //指令发送等待
    #define _CmdSendWait(_Cmd, _RepeatNum, _ReSendDelay)    /*指令发送等待*/  \
        do{ \
            ph->CurrentCmd = (_Cmd); \
            ph->Tx_State = _Tx_S_Init; \
            while(1){ \
                Ret = BM2166_CmdSendTimeoutLoopDis(ph, (_RepeatNum), (_ReSendDelay));   /*发送*/ \
                if(Ret < 0){  goto GOTO_Error; }    /*>错误*/ \
                else if(Ret == 0){ break; }         /*完成*/ \
                _XCCOR_SetBPBreak(phC);             /*跳出*/ \
            } \
        }while(0)
    //确认码错误跳转
    #define _CCodeErrorGoto()           if(CCode != _BM2166_CC_Succeed){ goto GOTO_Error; }

//=== 私有全局变量 ========================================|
//=== 组包数据 ============================================|
    //自动注册指令配置数据
    #define _CmdData_AutoEnroll_InNum   /*录入次数*/        (_BM2166_Cnd_AutoEnroll_InNum)
    #define _CmdData_AutoEnroll_Param0  /*自动注册,高位*/   (0x00)
    #define _CmdData_AutoEnroll_Param1  /*自动注册,低位*/   (0x00 | \
        (_BM2166_Cnd_AutoEnroll_LED   << 0) |   /*[B0];0常亮,1获取图像后熄灭*/ \
        (_BM2166_Cnd_AutoEnroll_Pre   << 1) |   /*[B1];预处理:0关闭,1开启*/ \
        (_BM2166_Cnd_AutoEnroll_Ret   << 2) |   /*[B2];步骤返回:0返回,1不返回*/ \
        (_BM2166_Cnd_AutoEnroll_Cover << 3) |   /*[B3];覆盖ID:0不允许,1允许*/ \
        (_BM2166_Cnd_AutoEnroll_ReReg << 4) |   /*[B4];重复注册:0允许,1不允许*/ \
        (_BM2166_Cnd_AutoEnroll_Leave << 5) |   /*[B5];多次注册是否要手指离开;0离开,1不离开*/ \
        (_BM2166_Cnd_AutoEnroll_Same  << 6) )   /*[B6];不同手指录入:0允许,1不允许*/

    //指令数据(默认值)(首字节是指令长度)
    static __ROM uchar r_CmdData[] = {
        //指令(只有部分指令)
            /*[0FH]读系统基本参数*/     0x02, 0x0F,                                 //固定
            /*[1FH]读索引表*/           0x03, 0x1F, 0x00,
            /*[34H]获取芯片唯一序列号*/ 0x03, 0x34, 0x00,                           //固定
            /*[35H]握手指令*/           0x02, 0x35,                                 //固定
            /*[36H]校验传感器*/         0x02, 0x36,                                 //固定
        //自动化指令(全部指令)
            /*[31H]自动注册模板*/       0x07, 0x31, 0x00, 0x00, _CmdData_AutoEnroll_InNum, _CmdData_AutoEnroll_Param0, _CmdData_AutoEnroll_Param1,
            /*[32H]自动验证指纹*/       0x07, 0x32, 0x03, 0xFF, 0xFF, 0x00, 0x07,   //安全等级3;ID:0xFFFF(1:N);LED获取图像成功后灭|打开预处理|注册过程不要求返回
            /*[0CH]删除模板*/           0x06, 0x0C, 0x00, 0x00, 0x00, 0x01,         //指纹ID0,删除一个
            /*[0DH]清空指纹库*/         0x02, 0x0D,                                 //固定
            /*[30H]取消指令*/           0x02, 0x30,                                 //固定
            /*[33H]休眠指令*/           0x02, 0x33,                                 //固定
            /*[1DH]读有效模板个数*/     0x02, 0x1D,                                 //固定
            /*[12H]设置口令*/           0x06, 0x12, 0xFF, 0xFF, 0xFF, 0xFF,         //口令全0xFF
            /*[13H]验证口令*/           0x06, 0x13, 0xFF, 0xFF, 0xFF, 0xFF,         //口令全0xFF
            /*[3CH]光环控制*/           0x06, 0x3C, 0xF6, 0x80, 0x01, 0x09,         //
    };



//=== 私有函数 ============================================|
    //数据
    static int BM2166_FindCmdData(uchar Cmd, uchar **pCmdData, ushort Len);  //从指令包中获取数据
    //发送
    static int BM2166_SendCmdPack(HandleBM2166 *ph);                        //发送指令
    static int BM2166_CmdSendTimeoutLoopDis(HandleBM2166 *ph, uchar RepeatNum, ulong ReSendDelay);  //指令发送超时处理

    //序列
    static int BM2166_Order(HandleBM2166 *ph);      //序列处理
    static int BM2166_OrderError(HandleBM2166 *ph);     //序列错误

    //接收
    static void BM2166_RxDecode(HandleBM2166 *ph);                          //接收解码
    static void BM2166_RxDataDis(HandleBM2166 *ph, uchar *pData);           //接收数据处理
    static int BM2166_RxAutoEnrollDis(HandleBM2166 *ph, uchar *pData);      //自动注册处理

    //其他控制函数
    static int BM2166_UpCurrentCmdTimeout(HandleBM2166 *ph, ulong ms);      //更新当前指令超时时间
    static int BM2166_FindIndex(uchar Index[32], ushort Num);               //搜索索引
    //弱函数
    extern void BM2166_WeakIO_VDD_EN(HandleBM2166 *ph);         //使能DSP
    extern void BM2166_WeakIO_VDD_DI(HandleBM2166 *ph);         //除能DSP
    extern void BM2166_WeakIO_UARTClose(HandleBM2166 *ph);      //关闭串口
    extern void BM2166_WeakIO_UARTOpen(HandleBM2166 *ph);       //打开串口
    extern void BM2166_WeakIO_UARTRecover(HandleBM2166 *ph);    //恢复串口
    extern void BM2166_WeakIO_SendData(HandleBM2166 *ph, uchar *pData, uint Len);   //发送数据

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/*=== 基本处理-说明*/
/************************************************|
 * 描述:    初始化
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_Init
 * 参数[H]: HandleBM2166 *ph
 * 参数[T]: HandleCBFIFO *phCBQ   //接收的队列
 * 参数[T]: TypeBM2166_fCB_Verify fVerify //接收验证回调
 * 参数[T]: TypeBM2166_fCB_Reg fReg   //注册回调
 * 参数[T]: TypeBM2166_fCB_Del fDel   //删除回调
 * 返回:    void
 *** 说明:  无
 *** 例程:  无
 ************************************************/
void BM2166_Init(HandleBM2166 *ph, ulong ICAddr, HandleCBFIFO *phCBQ, TypeBM2166_fCB_Verify fVerify, TypeBM2166_fCB_Reg fReg, TypeBM2166_fCB_Del fDel)
{
    //协程相关
    _XCCOR_Init(&ph->hCOR);             //协程句柄
    ph->CORTime = 0;                    //协程时间
    //芯片相关
    ph->ICAddr = ICAddr;                //芯片地址
    //通用数据
    ph->OrderCnf = 0;                   //序列控制
    ph->OrderState = _Order_S_NULL;     //状态
    ph->CurrentCmd = 0;                 //当前处理的指令
    //发送相关
    ph->Tx_RepeatNum = 0;               //重复发送次数
    ph->pTx_Param = NULL;               //发送用的参数([长度+指令+数据]*n)
    ph->Tx_ParamLen = 0;                //发送用参数长度
    _Mark_ClrAll(ph->Tx_BSem);
    //接收相关
    ph->phCBQ_Rx = phCBQ;               //接收_环形队列
    ph->Rx_Timeout = 0;                 //接收_超时
    ph->Rx_NextLen = _Len_PackHeadLen;  //接收_下个数据长度
    ph->Rx_FSMState = _RxFSM_P_00;      //接收_状态机状态
    ph->Rx_PackMark = 0;                //接收_包标识
    ph->Rx_PackLen = 0;                 //接收_包长度(数据+校验)
    //回调清除
    ph->fVerify = fVerify;              //接收验证回调
    ph->fReg = fReg;                    //注册回调
    ph->fDel = fDel;                    //删除回调
}

/************************************************|
 * 描述:    循环处理
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_Loop
 * 参数[H]: HandleBM2166 *ph
 * 返回:    int
 *  +=返回
 *  | >0    //运行中
 *  | _XCCOR_S_Finish   //[0]表示完成
 *  | -1    //失败
 *** 说明:  无
 *** 例程:  无
 ************************************************/
int BM2166_Loop(HandleBM2166 *ph)
{
    int Ret = 0;
//===
    if(ph->OrderState == _Order_S_Run){
        //序列处理
        Ret = BM2166_Order(ph);
        //接收处理
        BM2166_RxDecode(ph);
    }
    return(Ret);
}
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************|
 * 描述:    设置接收环形队列的句柄
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_SetRxCBFIFOHandle
 * 参数[H]: HandleBM2166 *ph
 * 参数[T]: HandleCBFIFO *phCBQ    //接收的队列
 * 返回:    void
 *** 说明:  无
 *** 例程:  无
 ************************************************/
void BM2166_SetRxCBFIFOHandle(HandleBM2166 *ph, HandleCBFIFO *phCBQ)
{
    ph->phCBQ_Rx = phCBQ;
}

/************************************************|
 * 描述:    设置校验回调
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_SetVerifyCallback
 * 参数[H]: HandleBM2166 *ph
 * 参数[F]: TypeBM2166_fCB_Verify fVerify
 * 返回:    void
 *** 说明:
 ***    回调形参顺序说明:
 ***        HandleBM2166 *ph //句柄
 ***        uchar Result    //结果
 ***        ushort ID       //ID号
 ***        ushort Score    //得分
 *** 例程:  无
 ************************************************/
void BM2166_SetVerifyCallback(HandleBM2166 *ph, TypeBM2166_fCB_Verify fVerify)
{
    ph->fVerify = fVerify;
}

/************************************************|
 * 描述:    设置注册回调
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_SetRegCallback
 * 参数[H]: HandleBM2166 *ph
 * 参数[F]: TypeBM2166_fCB_Reg fReg
 * 返回:    void
 *** 说明:
 ***    回调形参顺序说明:
 ***        HandleBM2166 *ph //句柄
 ***        int Param        //参数
 *** 例程:  无
 ************************************************/
void BM2166_SetRegCallback(HandleBM2166 *ph, TypeBM2166_fCB_Reg fReg)
{
    ph->fReg = fReg;
}


/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************|
 * 描述:    上电
 * 版本:    0.01
 * 日期:    2020/10/20
 * 函数名:  BM2166_PowerUp
 * 参数[H]: HandleBM2166 *ph
 * 返回:    void
 *** 说明:
 ***    操作是异步的
 *** 例程:  无
 ************************************************/
void BM2166_PowerUp(HandleBM2166 *ph)
{
    //协程复位
    _XCCOR_Init(&ph->hCOR);             //协程句柄
    ph->CORTime = 0;                    //协程时间
    //序列配置
    ph->OrderCnf =
        _OrderCnf_Mask_PowerUp |
        _OrderCnf_Mask_Handshake |
        _OrderCnf_Mask_CloseVDD;
    ph->OrderState = _Order_S_Run;      //序列运行
}

/************************************************|
 * 描述:    添加指纹
 * 版本:    0.01
 * 日期:    2020/10/20
 * 函数名:  BM2166_Add
 * 参数[H]: HandleBM2166 *ph
 * 参数[H]: ushort ID         //添加指纹的ID(无效指纹的话自动处理)
 * 返回:    void
 *** 说明:
 ***    操作是异步的
 *** 例程:  无
 ************************************************/
void BM2166_Add(HandleBM2166 *ph, ushort ID)
{
    //协程复位
    _XCCOR_Init(&ph->hCOR);             //协程句柄
    ph->CORTime = 0;                    //协程时间
    //序列配置
    ph->OrderCnf =
        _OrderCnf_Mask_WakeUp |
        _OrderCnf_Mask_Handshake |
        _OrderCnf_Mask_Add |
        _OrderCnf_Mask_CloseVDD;
    ph->OrderState = _Order_S_Run;      //序列运行
    //保存ID
    ph->Param_ID = ID;
}

/************************************************|
 * 描述:    验证指纹
 * 版本:    0.01
 * 日期:    2020/10/20
 * 函数名:  BM2166_Verify
 * 参数[H]: HandleBM2166 *ph
 * 返回:    void
 *** 说明:
 ***    操作是异步的
 *** 例程:  无
 ************************************************/
void BM2166_Verify(HandleBM2166 *ph)
{
    //协程复位
    _XCCOR_Init(&ph->hCOR);             //协程句柄
    ph->CORTime = 0;                    //协程时间
    //序列配置
    ph->OrderCnf =
        _OrderCnf_Mask_WakeUp |
        _OrderCnf_Mask_Handshake |
        _OrderCnf_Mask_Verify |
        _OrderCnf_Mask_CloseVDD;
    ph->OrderState = _Order_S_Run;      //序列运行
}

/************************************************|
 * 描述:    删除指纹
 * 版本:    0.01
 * 日期:    2020/10/20
 * 函数名:  BM2166_Del
 * 参数[H]: HandleBM2166 *ph
 * 参数[H]: ushort ID         //指纹的ID("_BM2166_P_NullID"的话先验证,在删除验证的指纹)
 * 返回:    void
 *** 说明:
 ***    操作是异步的
 *** 例程:  无
 ************************************************/
void BM2166_Del(HandleBM2166 *ph, ushort ID)
{
    //协程复位
    _XCCOR_Init(&ph->hCOR);             //协程句柄
    ph->CORTime = 0;                    //协程时间
    //序列配置
    ph->OrderCnf =
        _OrderCnf_Mask_WakeUp |
        _OrderCnf_Mask_Handshake |
        _OrderCnf_Mask_Del |
        _OrderCnf_Mask_CloseVDD;
    if(ID == _BM2166_P_NullID){
        ph->OrderCnf |= _OrderCnf_Mask_Verify;
    }
    ph->OrderState = _Order_S_Run;      //序列运行
    //保存ID
    ph->Param_ID = ID;
}


/************************************************|
 * 描述:    删除所有
 * 版本:    0.01
 * 日期:    2020/10/20
 * 函数名:  BM2166_DelAll
 * 参数[H]: HandleBM2166 *ph
 * 返回:    void
 *** 说明:
 ***    操作是异步的
 *** 例程:  无
 ************************************************/
void BM2166_DelAll(HandleBM2166 *ph)
{
    //协程复位
    _XCCOR_Init(&ph->hCOR);             //协程句柄
    ph->CORTime = 0;                    //协程时间
    //序列配置
    ph->OrderCnf =
        _OrderCnf_Mask_WakeUp |
        _OrderCnf_Mask_Handshake |
        _OrderCnf_Mask_DelAll |
        _OrderCnf_Mask_CloseVDD;
    ph->OrderState = _Order_S_Run;      //序列运行
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************|
 * 描述:    [私有]上电
 * 版本:    0.01
 * 日期:    2020/10/15
 * 函数名:  BM2166_Order_PowerUp
 * 参数[H]: HandleBM2166 *ph
 * 返回:    int
 *  +=返回
 *  | >0 运行中
 *  | _XCCOR_S_Finish   //表示完成
 *  | -1    //失败
 *** 说明:  无
 *** 例程:  无
 ************************************************/
static int BM2166_Order(HandleBM2166 *ph)
{
    int Ret;
    HandleXCCOR *phC;
//===
    phC = &ph->hCOR;
/*================================================*/
    _XCCOR_Start(phC);
/*=== 协程开始 ===================================*/
    _Debug_FPR("FPR start\r\n");
//=== 唤醒|上电
    if(ph->OrderCnf & _OrderCnf_Mask_WakeUp){
        BM2166_WeakIO_UARTClose(ph);                //关闭串口
        _Mark_Clr(ph->Tx_BSem, _TxBSem_RxSyncFrame);  //清除标志
        CBFIFO_Rst(ph->phCBQ_Rx);
        BM2166_WeakIO_VDD_EN(ph);                   //使能芯片
        _XCCOR_Delay_ms(phC, ph->CORTime, 10);      //延时10ms
        BM2166_WeakIO_UARTOpen(ph);                 //开串口
        //同步帧等待
        ph->CORTime = _Time_GetTick_ms(200);                //更新时间(官方推荐200ms)
        while(!_Time_CompareTick_t(ph->CORTime)){           //|时间没有到
            if(_Mark_Get(ph->Tx_BSem, _TxBSem_RxSyncFrame)){  //|收到数据标志
                break;
            }
            _XCCOR_SetBPBreak(phC);                 //跳出
        }
        //清除标志
        ph->OrderCnf &= (~_OrderCnf_Mask_WakeUp);   //清除标志
        _Debug_FPR("FPR VDD Open\r\n");
    }
//=== 握手
    if(ph->OrderCnf & _OrderCnf_Mask_Handshake){
        _Debug_FPR("FPR Handshake\r\n");
        _CmdSendWait(_BM2166_Cmd_Handshake, 3, 300);
    }
//=== 校验
    if(ph->OrderCnf & _OrderCnf_Mask_Verify){
        _Debug_FPR("FPR Verify AutoIdentify\r\n");
        _CmdSendWait(_BM2166_Cmd_AutoIdentify, 1, 11000);   //验证指令超时官方默认10s
        _XCCOR_Delay_ms(phC, ph->CORTime, 500);      //延时(等指纹灯)
    }
//=== 添加
    if(ph->OrderCnf & _OrderCnf_Mask_Add){
        if(ph->Param_ID != _BM2166_P_NullID){       //|手动给指纹ID
            //读系统基本参数(读指纹容量)
            _Debug_FPR("FPR ReadSysPara\r\n");
            _CmdSendWait(_BM2166_Cmd_ReadSysPara, 2, 500);

            //读索引表,循环读
            for(ph->Buf[2]; ph->Buf[2]<=ph->Buf[3]; ph->Buf[2]++){
                _Debug_FPR("FPR ReadIndexTable num:%u\r\n", ph->Buf[2]);
                _CmdSendWait(_BM2166_Cmd_ReadIndexTable, 2, 500);
            }
        }
        //自动注册
        _Debug_FPR("FPR Add AutoEnroll ID:%u\r\n", ph->Param_ID);
        _CmdSendWait(_BM2166_Cmd_AutoEnroll, 1, 12000);     //指纹默认超时是10s
    }
//=== 删除
    if(ph->OrderCnf & _OrderCnf_Mask_Del){
        _Debug_FPR("FPR Del DeletChar\r\n");
        _CmdSendWait(_BM2166_Cmd_DeletChar, 1, 300);
    }
//=== 删除全部
    if(ph->OrderCnf & _OrderCnf_Mask_DelAll){
        _Debug_FPR("FPR DelAll Empty\r\n");
        _CmdSendWait(_BM2166_Cmd_Empty, 1, 300);
    }
//=== 关闭电源
    if(ph->OrderCnf & _OrderCnf_Mask_CloseVDD){
        if(0){
        GOTO_Error:
            _Mark_Clr(ph->Tx_BSem, _TxBSem_Error);  //清除错误
            Ret = BM2166_OrderError(ph);
            if(Ret == -2){
                goto GOTO_CloseVDD;                 //调到关闭电源
            }
            //取消指令
            _Debug_FPR("FPR Cancel\r\n");
            _CmdSendWait(_BM2166_Cmd_Cancel, 2, 300);
        }
        //休眠指令
        _Debug_FPR("FPR Sleep\r\n");
        _CmdSendWait(_BM2166_Cmd_Sleep, 2, 300);
        //关闭
    GOTO_CloseVDD:
        _Debug_FPR("FPR CloseVDD\r\n");
        BM2166_WeakIO_UARTClose(ph);
        _XCCOR_Delay_ms(phC, ph->CORTime, 15);      //延时10ms
        BM2166_WeakIO_VDD_DI(ph);                   //关闭芯片
        _XCCOR_Delay_ms(phC, ph->CORTime, 10);      //延时10ms
        BM2166_WeakIO_UARTRecover(ph);              //恢复串口
        _Debug_FPR("FPR VDD Off\r\n");
    }
    ph->OrderCnf = 0;                               //序列配置清除
    ph->OrderState = _Order_S_NULL;                 //状态清除
    _Debug_FPR("FPR End\r\n");
/*=== 协程结束 ===================================*/
    _XCCOR_Finish(phC);                 //协程完成
    _XCCOR_End(phC);                    //协程结束
    /*结束处理*/
    return(_XCCOR_GetState(phC));
/*=== 错误 =======================================*/
}

/************************************************|
 * 描述:    [私有]指令发送超时处理
 * 版本:    0.01
 * 日期:    2020/10/15
 * 函数名:  BM2166_CmdSendTimeoutDis
 * 参数[H]: HandleBM2166 *ph
 * 参数[I]: uchar RepeatNum       //重复次数
 * 参数[I]: ulong ReSendDelay     //每次重复后延时
 * 返回:    int
 *  +=反馈
 *  | -2    //错误
 *  | -1    //超时
 *  |  0    //完成
 *  |  1    //运行中
 *** 说明:  无
 *** 例程:  无
 ************************************************/
static int BM2166_CmdSendTimeoutLoopDis(HandleBM2166 *ph, uchar RepeatNum, ulong ReSendDelay)
{
//    int Ret;
//===
    if(ph->Tx_State == _Tx_S_Init){
        _Mark_Clr(ph->Tx_BSem, _TxBSem_ACK);        //清除ACK
        _Mark_Clr(ph->Tx_BSem, _TxBSem_Error);      //清除错误
        ph->Tx_RepeatNum = RepeatNum;
        ph->Tx_State = _Tx_S_Send;
    }
    //发送
    if(ph->Tx_State == _Tx_S_Send){
        BM2166_SendCmdPack(ph);                         //发送
        ph->CORTime = _Time_GetTick_ms(ReSendDelay);    //更新时间
        ph->Tx_State = _Tx_S_WaitACK;
        return(1);
    }
    //等待ACK
    else{
        //错误判断
        if(_Mark_Get(ph->Tx_BSem, _TxBSem_Error)){
            return(-2);
        }
        //超时判断
        if(!_Time_CompareTick_t(ph->CORTime)){          //|时间没有到
            if(_Mark_Get(ph->Tx_BSem, _TxBSem_ACK)){    //|收到数据标志
                return(0);                              //=完成
            }
        }
        else{   //|超时
            if(--ph->Tx_RepeatNum == 0){
                return(-1);                         //=超次数
            }
            else{
                ph->Tx_State = _Tx_S_Send;
            }
        }
    }
    return(1);
}

/************************************************|
 * 描述:    [私有]流程错误处理
 * 版本:    0.01
 * 日期:    2020/10/15
 * 函数名:  BM2166_OrderError
 * 参数[H]: HandleBM2166 *ph
 * 返回:    int
 *  +=返回
 *  | -1    //跳到取消指令
 *  | -2    //跳到关闭电源
 *** 说明:  无
 *** 例程:  无
 ************************************************/
static int BM2166_OrderError(HandleBM2166 *ph)
{
    int Ret;
//===
    _Debug_FPR("FPR Error Cmd:%02XH\r\n",ph->CurrentCmd);
    Ret = -1;
    switch(ph->CurrentCmd){
        case _BM2166_Cmd_Cancel:    //|取消指令
        case _BM2166_Cmd_Sleep:     //|休眠指令
            Ret = -2;
            break;
        default:
            break;
    }
//===
    return(Ret);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 其他控制函数
/************************************************|
 * 描述:    [私有]更新当前指令超时时间
 * 版本:    0.01
 * 日期:    2020/10/26
 * 函数名:  BM2166_UpCurrentCmdTimeout
 * 参数[H]: HandleBM2166 *ph
 * 参数[I]: ulong ms
 * 返回:    int
 *  +=返回状态
 *  | _BM2166_R_OK      //成功
 *  | _BM2166_R_Fail    //失败,时间已经超时
 *** 说明:
 ***    调用次函数后,时间是从当前Tick开始计算的;
 *** 例程:  无
 ************************************************/
static int BM2166_UpCurrentCmdTimeout(HandleBM2166 *ph, ulong ms)
{
    if(!_Time_CompareTick_t(ph->CORTime)){          //时间没有到
        ph->CORTime = _Time_GetTick_ms(ms);         //更新时间
        return(_BM2166_R_OK);
    }
    return(_BM2166_R_Fail);
}


/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************|
 * 描述:    [私有]接收解析
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_RxDecode
 * 参数[H]: HandleBM2166 *ph
 * 返回:    int
 *** 说明:  无
 *** 例程:  无
 ************************************************/
static void BM2166_RxDecode(HandleBM2166 *ph)
{
    uint Len;
    HandleCBFIFO *phQ;
    uchar Buf[_BM22166_Cnf_RxBufLen];
    ulong Cache32;
    ushort C16N1,C16N2;
    ulong i;
//===
    phQ = ph->phCBQ_Rx;
    Len = CBFIFO_GetUseBufSize(phQ);                //得到数据长度
    if(Len == 0){ return; }                         //=无数据返回
//=== 特殊唤醒同步值
    if(ph->OrderCnf & _OrderCnf_Mask_WakeUp){       //需要唤醒
        CBFIFO_ShiftRead(phQ, 0, Buf, 1);           //只读-数据
        if(Buf[0] == _RxSyncFrame){
            _Mark_Set(ph->Tx_BSem, _TxBSem_RxSyncFrame);
            _Debug_FPR("FPR Rx SyncFrame\r\n");
        }
    }
//=== 解析
    //数据长度|超时判断
    if(Len < ph->Rx_NextLen){
        //数据不足
        if(ph->Rx_Timeout++ > _RxTimeoutCount){
            goto GOTO_End;                      //>超时
        }
        return;                                 //=数据长度无效
    }
    //_Debug_FPR("FPR Rx len:%u\r\n",Len);
    ph->Rx_Timeout = 0;
    //解析状态机
    switch(ph->Rx_FSMState){
        //|包头判断(包头(2B))
        case _RxFSM_P_00:
            //_Debug_FPR("FPR Rx data 00\r\n");
            CBFIFO_ShiftByte(phQ, Buf);         //出队列-字节
            //_Debug_FPR("FPR Rx State 00; %02X\r\n", Buf[0]);
            if(Buf[0] == _PackHead0){
                CBFIFO_ShiftRead(phQ, 0, Buf, 1);   //只读-数据
                if(Buf[0] == _PackHead1){           //|成功
                    CBFIFO_ShiftDiscard(phQ, 1);    //抛弃一个
                    ph->Rx_NextLen = _Len_InfoLen;  //下个长度
                    ph->Rx_FSMState = _RxFSM_P_01;  //下个状态
                }
            }
            break;
        //|信息判断(芯片地址(4B)+包标识(1B)+包长度(2B))
        case _RxFSM_P_01:
            //_Debug_FPR("FPR Rx State 01\r\n");
            CBFIFO_ShiftRead(phQ, 0, Buf, _Len_InfoLen);    //只读-数据
            Cache32 = (Buf[0]<<24) | (Buf[1]<<16) | (Buf[2]<<8) | (Buf[3]);
            if(Cache32 != ph->ICAddr){          //地址错误
                goto GOTO_End;
            }
            //标识判断
            if( !( (Buf[4] == _PackMark_Cmd) |
                   (Buf[4] == _PackMark_Data) |
                   (Buf[4] == _PackMark_EndData) |
                   (Buf[4] == _PackMark_ACK) ) ){
                goto GOTO_End;
            }
            ph->Rx_PackMark = Buf[4];           //包标识
            //长度处理(数据+校验)
            C16N1 = (Buf[5]<<8) | (Buf[6]);     //得到长度
            ph->Rx_PackLen = C16N1;             //保存长度
            //下个
            ph->Rx_NextLen += C16N1;            //下个长度
            ph->Rx_FSMState = _RxFSM_P_02;      //下个状态
            break;
        //|数据(数据+校验)
        case _RxFSM_P_02:
            //_Debug_FPR("FPR Rx State 02\r\n");
            CBFIFO_ShiftRead(phQ, _Len_InfoLen, Buf, ph->Rx_PackLen);   //只读-数据
            //校验(包标识+包长度+数据)
            C16N1 = ph->Rx_PackMark + (ph->Rx_PackLen & 0xFF) + ((ph->Rx_PackLen>>8) & 0xFF);
            C16N2 = ph->Rx_PackLen-2;               //数据长度
            for(i=0; i<C16N2; i++){
                C16N1 += Buf[i];
            }
            Cache32 = (Buf[C16N2++]<<8);
            Cache32 += Buf[C16N2];

            if(C16N1 != Cache32){                   //|校验错误
                _Debug_FPR("FPR Rx data verify Error, %04X:%04X\r\n",C16N1,Cache32);
                goto GOTO_End;                      //>校验错误
            }
            //数据处理
            BM2166_RxDataDis(ph, Buf);              //输入的数据是(数据+校验),长度是"ph->Rx_PackLen"
            //丢弃数据
            C16N1 = ph->Rx_PackLen+_Len_InfoLen;
            CBFIFO_ShiftDiscardExt(phQ, &C16N1);    //出队列-抛弃
        default:
            goto GOTO_End;
    }
//=== 结束处理
    return;
GOTO_End:
    ph->Rx_FSMState = _RxFSM_P_00;              //接收状态机初始状态
    ph->Rx_Timeout = 0;                         //接收包超时
    ph->Rx_NextLen = _Len_PackHeadLen;          //初始判断长度是包头长度
}

/************************************************|
 * 描述:    [私有]接收数据处理
 * 版本:    0.01
 * 日期:    2020/10/13
 * 函数名:  BM2166_RxDataDis
 * 参数[H]: HandleBM2166 *ph
 * 参数[H]: uchar *pData          //数据(确认码+数据)
 * 返回:    void
 *** 说明:  无
 *** 例程:  无
 ************************************************/
static void BM2166_RxDataDis(HandleBM2166 *ph, uchar *pData)
{
#if(_Debug_FPR_EN == 1)
    _DebugDT("FRP Rx pack[%02XH],data:", ph->CurrentCmd);
//    _DebugDT("|Cmd: %02XH\r\n", ph->CurrentCmd);
//    _DebugDT("|data:");
    for(int i=0; i<(ph->Rx_PackLen-2); i++){
        _Debug(" %02X", pData[i]);
    }
    _Debug("\r\n");
#endif
//===
    uchar CCode;            //确认码|反馈码
    ushort Score;           //指纹得分
    int Ret;
    ushort C16N1,C16N2;
//=== 对应指令处理
    //ACK反馈
    if(ph->Rx_PackMark == _PackMark_ACK){
        _Mark_Set(ph->Tx_BSem, _TxBSem_ACK);          //设置ACK
        CCode = pData[0];    //确认码
        ph->Param_CCode = CCode;

        //指令处理
        switch(ph->CurrentCmd){
            //|自动验证指纹
            case _BM2166_Cmd_AutoIdentify:
                ph->Param_ID = (pData[2]<<8) | pData[3];    //指纹ID
                if(ph->OrderCnf & _OrderCnf_Mask_Del){      //|需要删除指纹
                    /*对应手指删除需要先得到指纹ID,这边预留*/
                }
                else if(ph->fVerify != NULL){
                    Score = (pData[4]<<8) | pData[5];
                    ph->fVerify(ph, Score);
                }
                break;
            //|自动注册模板
            case _BM2166_Cmd_AutoEnroll:
#if(_BM2166_Cnd_AutoEnroll_Ret == 0)
                //有注册反馈步骤的才处理
                Ret = BM2166_RxAutoEnrollDis(ph, pData);
                if(Ret < 0){                        //|错误
                    goto GOTO_Error;
                }
                else if(Ret != 0){                  //|没有完成
                    //指纹注册是多步骤,这边关闭ACK返回,全部完后才返回
                    _Mark_Clr(ph->Tx_BSem, _TxBSem_ACK);
                }
                else{                               //|完成
                    /*完成注册,不关闭ACK标志*/
                }
#endif
                break;
            //|删除单个模板
            case _BM2166_Cmd_DeletChar:
                if(ph->fDel != NULL){
                    ph->fDel(ph, (CCode==_BM2166_CC_Succeed) ? ph->Param_ID : _BM2166_DelType_Error);
                }
                break;
            //|清空指纹库
            case _BM2166_Cmd_Empty:
                if(ph->fDel != NULL){
                    ph->fDel(ph, (CCode==_BM2166_CC_Succeed) ? _BM2166_DelType_All : _BM2166_DelType_AllError);
                }
                break;
            //|读索引表
            case _BM2166_Cmd_ReadIndexTable:
                _CCodeErrorGoto();
                C16N1 = (ph->Buf[0]<<8) | ph->Buf[1];       //数量
                C16N2 = (C16N1>(32*8)) ? (32*8) : C16N1;    //当前搜索的量
                Ret = BM2166_FindIndex(&pData[1], C16N2);   //搜索索引
                if(Ret >= 0){                       //|搜到
                    ph->Buf[2] = 1;
                    ph->Buf[3] = 0;
                    ph->Param_ID += Ret;            //得到ID
                }
                else{
                    ph->Param_ID += (32*8);         //增加搜索的值
                    C16N1 -= C16N2;
                    ph->Buf[0] = C16N1>>8;
                    ph->Buf[1] = C16N1;
                    if(C16N1 == 0){
                        goto GOTO_Error;
                    }
                }
                break;
            //|读系统基本参数
            case _BM2166_Cmd_ReadSysPara:
                _CCodeErrorGoto();
                //得到容量
                ph->Buf[0] = pData[5];
                ph->Buf[1] = pData[6];
                //清除参数
                C16N1 = (pData[5]<<8) | pData[6];   //数量
                C16N1 += 0xFE;
                C16N1 >>= 8;
                ph->Buf[2] = 0;                     //用作读索引计数
                ph->Buf[3] = C16N1;                 //得到读索引次数
                ph->Param_ID = 0;                   //清除ID
                _Debug_FPR("FPR size:%u;%u\r\n", (pData[5]<<8) | pData[6], ph->Buf[3]);
                break;
            default:
                break;
        }
    }
//===
    return;
//=== 错误
GOTO_Error:
    _Mark_Set(ph->Tx_BSem, _TxBSem_Error);
}

/************************************************|
 * 描述:    [私有]接收指纹自动注册处理
 * 版本:    0.01
 * 日期:    2020/10/13
 * 函数名:  BM2166_RxAutoEnrollDis
 * 参数[H]: HandleBM2166 *ph
 * 参数[H]: uchar *pData          //数据(3字节:确认码+参数1+参数2)
 * 返回:    int
 *  +=返回
 *  | _BM2166_R_GoOn    //继续
 *  | _BM2166_R_OK      //成功完成
 *  | _BM2166_R_Fail    //失败
 *** 说明:
 ***    默认4次注册得到数据是(确认码,状态码,反馈码):
 ***        00 00 00    //指令发送后就返回
 ***        00 01 01
 ***        00 02 01    //第一次完成
 ***        00 03 01    //抬起手指
 ***        00 01 02
 ***        00 02 02    //第二次完成
 ***        00 03 02
 ***        00 01 03
 ***        00 02 03    //第三次完成
 ***        00 03 03
 ***        00 01 04
 ***        00 02 04    //第四次完成
 ***        00 04 F0    //合并采集的的数据成模板
 ***        00 05 F1    //验证模板是否存在
 ***        00 06 F2    //保存模板
 *** 例程:  无
 ************************************************/
static int BM2166_RxAutoEnrollDis(HandleBM2166 *ph, uchar *pData)
{
    uchar CCode;        //确认码
    uchar State;        //状态(参数1)
    uchar FBack;        //反馈(参数2)
    int Ret;
    int CB_Param;
//===
    CCode = pData[0];
    State = pData[1];
    FBack = pData[2];
    Ret = _BM2166_R_GoOn;       //默认继续
    CB_Param = 999;
//=== 错误判断
    if(CCode != _BM2166_CC_Succeed){  //|错误
        Ret = _BM2166_R_Fail;
        CB_Param = _BM2166_AddState_Error;
        goto GOTO_CB;
    }
//=== 流程处理
    if( (State == _AutoEnroll_State_Validity) && (FBack == _AutoEnroll_FBack_Validity) ){   //|开始
        CB_Param = _BM2166_AddState_Start;          //开始
    }
    else if(State == _AutoEnroll_State_GenChar){    //|生成特征值,完成一次按手指
        CB_Param = FBack;                           //当前次数
    }
    else if(State == _AutoEnroll_State_Save){       //|存储模板
        CB_Param = _BM2166_AddState_Finish;         //完成
        Ret = _BM2166_R_OK;                         //返回完成
    }
    BM2166_UpCurrentCmdTimeout(ph, 12*1000);        //更新时间
//=== 回调处理
GOTO_CB:
    if( (ph->fReg != NULL) && (CB_Param != 999) ){
        ph->fReg(ph, CB_Param);
    }
//===
    return(Ret);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 组包
/************************************************|
 * 描述:    [私有]发送指令包
 * 版本:    0.01
 * 日期:    2020/10/13
 * 函数名:  BM2166_SendCmdPack
 * 参数[H]: HandleBM2166 *ph
 * 返回:    int
 *  +=说明
 *  | >=0   //指令长度
 *  | _BM2166_R_CmdNotExist     //指令不存在
 *** 说明:  无
 *** 例程:  无
 ************************************************/
static int BM2166_SendCmdPack(HandleBM2166 *ph)
{
    uchar Buf[_BM22166_Cnf_SendBufLen];
    uchar *pCmdData;
    ushort CmdDataLen;
    uchar CmdLen;
    uint  PackLen;
    ushort Cache16;
    ushort i;
    uchar Cmd;
//===
    //包头
    Buf[0] = _PackHead0;
    Buf[1] = _PackHead1;
    //芯片地址
    Buf[2] = ph->ICAddr>>24;
    Buf[3] = ph->ICAddr>>16;
    Buf[4] = ph->ICAddr>>8;
    Buf[5] = ph->ICAddr;
    //包标识
    Buf[6] = _PackMark_Cmd;
    //包长度
//    Buf[7]
//    Buf[8]
//=== 查找数据
    Cmd = ph->CurrentCmd;
    //从句柄中数据查找
    if(ph->pTx_Param != NULL){
        pCmdData = ph->pTx_Param;
        CmdDataLen = ph->Tx_ParamLen;
        CmdLen = BM2166_FindCmdData(Cmd, &pCmdData, CmdDataLen);
        if(CmdLen != 0){
            goto GOTO_CmdProcessing;
        }
    }
    //默认数据表中查找
    pCmdData = (uchar*)r_CmdData;
    CmdDataLen = _ArraySize(r_CmdData);
    CmdLen = BM2166_FindCmdData(Cmd, &pCmdData, CmdDataLen);
    if(CmdLen != 0){
        goto GOTO_CmdProcessing;
    }
    return(_BM2166_R_CmdNotExist);                  //=指令不存在
//=== 准备指令
GOTO_CmdProcessing:
    memcpy(&Buf[9], pCmdData, CmdLen);              //加载数据
    //加载特殊数据
    if( (Cmd == _BM2166_Cmd_AutoEnroll) ||          //|自动注册
        (Cmd == _BM2166_Cmd_DeletChar) ){           //|删除指纹
        Buf[10] = ph->Param_ID>>8;
        Buf[11] = ph->Param_ID;
    }
//=== 校验和包长度
//=== 包长度是:数据和校验的长度(不包含长度字节)
//=== 校验是:"包标识+包长度+数据"所有字节之和,超出2字节的进位忽略;
    Cache16 = 0;
    PackLen = CmdLen+2;             //包长度(数据+校验)
    Buf[7] = PackLen>>8;
    Buf[8] = PackLen;
    PackLen += (2+4+1+2-2);         //除校验外的所有长度(包头+芯片地址+包标识+包长度+数据)
    for(i=6; i<PackLen; i++){
        Cache16 += Buf[i];
    }
    //校验值
    Buf[PackLen++] = Cache16>>8;
    Buf[PackLen++] = Cache16;
//=== 发送
#if(_Debug_FPR_EN == 1)
    _DebugDT("FRP Send pack[%02XH],data:", Buf[9]);
//    _DebugDT("|Cmd: %02XH\r\n", Buf[9]);
//    _DebugDT("|data:");
    for(int i=0; i<PackLen; i++){
        _Debug(" %02X", Buf[i]);
    }
    _Debug("\r\n");
#endif

    BM2166_WeakIO_SendData(ph, Buf, PackLen);
//===
    return(PackLen);                //=返回包总长度
}

/************************************************|
 * 描述:    [私有]从数据包中查找对应数据
 * 版本:    0.01
 * 日期:    2020/10/19
 * 函数名:  BM2166_FindCmdData
 * 参数[I]: uchar Cmd         //查找的指令
 * 参数[IO]:uchar *pCmdData   //指令数据(输入整个指令数据,输出找到的指令数据(包含指令))
 * 参数[I]: ushort Len        //指令数据长度(整个"pCmdData"长度)
 * 返回:    int
 *  +=返回
 *  | >0    //找到数据,并返回长度
 *  |  0    //没有找到数据
 *** 说明:
 ***    没有找到指令不会修改"pCmdData"参数;
 *** 例程:  无
 ************************************************/
static int BM2166_FindCmdData(uchar Cmd, uchar **pCmdData, ushort Len)
{
    ushort i;
    uchar OutLen;
//===
    i = 0;
    do{
        if(Cmd == (*pCmdData)[i+1]){                    //|得到指令
            OutLen = (*pCmdData)[i]-1;
            *pCmdData = &(*pCmdData)[i+1];              //指令指针
            return(OutLen);                             //指令长度(不包含首字节长度)
        }
        i += (*pCmdData)[i];
    }while(i<Len);

    return(0);
}

/************************************************|
 * 描述:    [私有]搜索索引得到ID
 * 版本:    0.01
 * 日期:    2020/10/26
 * 函数名:  BM2166_FindIndex
 * 参数[I]: uchar Index[32]   //索引
 * 参数[I]: ushort Num        //数量
 * 返回:    int
 *  +=返回
 *  | >=0   //得到的ID值
 *  | -1    //没有搜索到
 *** 说明:
 ***
 *** 例程:  无
 ************************************************/
static int BM2166_FindIndex(uchar Index[32], ushort Num)
{
    uchar i,j;
    uchar Cache8;
    ushort Count;
//===
    Count = 0;
    for(j=0; j<32; j++){
        Cache8 = Index[j];
        for(i=0; i<8; i++){
            if( (Cache8 & 0x01) == 0 ){             //|有空位,找到
                return(Count);
            }
            Cache8 >>= 1;                           //下个空位
            if(++Count >= Num){
                return(-1);                         //=没有找到
            }
        }
    }
    return(-1);                                     //=没有找到
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************ 我是分割线 ************************************************/
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
#if(_BM2166_WeakFunc_EN==1)
/*=== 弱函数实现-说明*/
/************************************************|
 * 描述:    [弱函数IO] 模块DSP使能
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_WeakIO_VDD_EN
 * 参数[H]: HandleBM2166 *ph
 * 返回:    void
 *** 说明:
 ***    模块DSP使能,接LDO使能脚(一般高电平使能)
 ***   *注意:上电前必须保证串口2个引脚为低电平,启动10ms后才可以收发数据(开启串口);
 *** 例程:  无
 ************************************************/
__WEAK void BM2166_WeakIO_VDD_EN(HandleBM2166 *ph)
{}

/************************************************|
 * 描述:    [弱函数IO] 模块DSP除能
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_WeakIO_VDD_DI
 * 参数[H]: HandleBM2166 *ph
 * 返回:    void
 *** 说明:
 ***    模块DSP除能,接LDO使能脚(一般低电平除能)
 ***   *注意:下电前先将串口2个引脚设置为低电平,等待10ms后在将模块电源拉低;
 *** 例程:  无
 ************************************************/
__WEAK void BM2166_WeakIO_VDD_DI(HandleBM2166 *ph)
{}

/************************************************|
 * 描述:    [弱函数IO] 关闭串口
 * 版本:    0.01
 * 日期:    2020/10/15
 * 函数名:  BM2166_WeakIO_UARTClose
 * 参数[H]: HandleBM2166 *ph
 * 返回:    void
 *** 说明:
 ***    指纹启动装,关闭串口,将串口2个引脚拉低;
 *** 例程:  无
 ************************************************/
__WEAK void BM2166_WeakIO_UARTClose(HandleBM2166 *ph)
{}

/************************************************|
 * 描述:    [弱函数IO] 打开串口
 * 版本:    0.01
 * 日期:    2020/10/15
 * 函数名:  BM2166_WeakIO_UARTOpen
 * 参数[H]: HandleBM2166 *ph
 * 返回:    void
 *** 说明:
 ***    开启串口;
 *** 例程:  无
 ************************************************/
__WEAK void BM2166_WeakIO_UARTOpen(HandleBM2166 *ph)
{}

/************************************************|
 * 描述:    [弱函数IO] 恢复串口
 * 版本:    0.01
 * 日期:    2020/10/15
 * 函数名:  BM2166_WeakIO_UARTRecover
 * 参数[H]: HandleBM2166 *ph
 * 返回:    void
 *** 说明:
 ***    所有指纹操作完成,可以将串口配置成其他;
 *** 例程:  无
 ************************************************/
__WEAK void BM2166_WeakIO_UARTRecover(HandleBM2166 *ph)
{}


/************************************************|
 * 描述:    [弱函数IO] 发送数据
 * 版本:    0.01
 * 日期:    2020/10/10
 * 函数名:  BM2166_WeakIO_SendData
 * 参数[H]: HandleBM2166 *ph
 * 形参[I]: uchar *pData    //数据
 * 形参[I]: uint Len        //数据长度
 * 返回:    void
 *** 说明:
 ***    发送数据给指纹头;
 *** 例程:  无
 ************************************************/
__WEAK void BM2166_WeakIO_SendData(HandleBM2166 *ph, uchar *pData, uint Len)
{}


/************************************************|
 * 描述:    [弱函数]-其他函数
 * 版本:    0.01
 * 日期:
 * 函数名:
 * 形参[N]: void
 * 返回:    void
 *** 说明:
 *** 例程:  无
 ************************************************/
//__WEAK void 文件名_Weak_功能(void)
//{
//}
//===
#endif

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
