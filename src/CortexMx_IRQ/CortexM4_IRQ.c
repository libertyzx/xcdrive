/*=========================================================|
 | 文件名: CortexM4_IRQ.c
 | 描述  : M4内核中断服务函数
 | 版本  : V0.01
 | 日期  : 2016/10/09
 | 语言  : C语言
 | 作者  : libertyzx
 | E-mail: libertyzx@163.com
 +-----------------------------------------------|
 +--- 说明
 |  1.内核中断处理函数全写这里;
 |  2.内核中断服务函数不需要声明;
 |  3.使用时直接复制到工程即可;
 +-----------------------------------------------|
 +--- 版本说明
 |  V0.1:-2016/10/09
 |      1.初始化代码
 *========================================================*/
//=== 头文件
    #include "Includes.h"
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 本文件宏定义 ========================================
//=== 本文件全局变量 ======================================
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************|
 * 描述:    不可屏蔽中(外部NMI输入)
 * 版本:    V0.01
 * 日期:    2016/10/09
 * 函数名:  NMI_Handler
 * 参数:    void
 * 返回:    void
 *** 说明:
 ***    优先级(不可设置): -14
 ***    由MCU厂家决定连接到哪里;
 *** 例程: 无
 ************************************************/
void NMI_Handler(void)
{
    while(1);
}

/************************************************|
 * 描述:    硬件错误
 * 版本:    V0.1
 * 日期:    2016/10/09
 * 函数名:  HardFault_Handler
 * 参数:    void
 * 返回:    void
 *** 说明 :
 *** 说明:
 ***    优先级(不可设置): -13
 ***    硬件错误;
 *** 例程: 无
 ************************************************/
void HardFault_Handler(void)
{
    while(1);
}

/************************************************|
 * 描述:    存储器管理错误
 * 版本:    V0.1
 * 日期:    2016/10/09
 * 函数名:  MemManage_Handler
 * 参数:    void
 * 返回:    void
 *** 说明:
 ***    优先级(可设置): -12
 ***    MPU 访问违例以及访问非法位置均可引发;
 ***    企图在"非执行区"取指也会引发此 fault;
 ************************************************/
void MemManage_Handler(void)
{
    while(1);
}

/************************************************|
 * 描述:    总线错误
 * 版本:    V0.1
 * 日期:    2016/10/09
 * 函数名:  BusFault_Handler
 * 参数:    void
 * 返回:    void
 *** 说明:
 ***    优先级(可设置): -11
 ***    从总线系统收到了错误响应,
 ***    原因可以是预取流产(Abort)或数据流产,
 ***    企图访问协处理器也会引发此 fault;
 ************************************************/
void BusFault_Handler(void)
{
    while(1);
}

/************************************************|
 * 描述:    使用错误
 * 版本:    V0.1
 * 日期:    2016/10/09
 * 函数名:  UsageFault_Handler
 * 参数:    void
 * 返回:    void
 *** 说明:
 ***    优先级(可设置): -10
 ***    由于程序错误导致的异常;
 ***    通常是使用了一条无效指令,或者是非法的状态转换,
 ***    例如尝试切换到 ARM 状态;
 ************************************************/
void UsageFault_Handler(void)
{
    while(1);
}

/************************************************|
 * 描述:    请求管理调用
 * 版本:    V0.1
 * 日期:    2016/10/09
 * 函数名:  SVC_Handler
 * 参数:    void
 * 返回:    void
 *** 说明:
 ***    优先级(可设置): -5
 ***    一般用于OS环境且允许应用任务访问系统服务;
 ************************************************/
void SVC_Handler(void)
{
    while(1);
}

/************************************************|
 * 描述:    调试监控
 * 版本:    V0.1
 * 日期:    2016/10/09
 * 函数名:  DebugMon_Handler
 * 参数:    void
 * 返回:    void
 *** 说明 :
 ***    优先级(可设置): -4
 ***    调试监视器(断点,数据观察点,或者是外部调试请求);
 ************************************************/
void DebugMon_Handler(void)
{
    while(1);
}

/************************************************|
 * 描述:    可挂起的服务调用
 * 版本:    V0.1
 * 日期:    2016/10/09
 * 函数名:  PendSV_Handler
 * 参数:    void
 * 返回:    void
 *** 说明:
 ***    优先级(可设置): -2
 ***    OS一般用该异常进行上下文切换;
 ************************************************/
void PendSV_Handler(void)
{
    while(1);
}

/************************************************|
 * 描述:    滴答定时器中断
 * 版本:    V0.1
 * 日期:    2016/10/09
 * 函数名:  SysTick_Handler
 * 参数:    void
 * 返回:    void
 *** 说明:
 ***    优先级(可设置): -1
 ***    滴答定时器溢出中断
 ************************************************/
void SysTick_Handler(void)
{
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
