/*=========================================================|
 | 文件名:  BootCortexMx.h
 | 描述:    CortexMx启动处理-函数声明
 | 版本:    V0.02
 | 日期:    2023/03/13
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail:  libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.CortexMx启动处理
 +-----------------------------------------------|
 +--- 版本说明
 |  V0.01:-2021/07/29
 |      1.初始
 |  V0.02:-2023/03/13
 |      1.兼容"M3"和"M4"内核
 *========================================================*/
//=== 防重复定义
#ifndef _BootCortexMx_H_
#define _BootCortexMx_H_
//=== 头文件
    /*这里不调用"core_cmx"头文件,调用此头文件前先调用对应内核头文件即可*/

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 数据配置宏 ==========================================|
    /*关中断等级
     *  等级0,留"NMI_Handler"和"HardFault_Handler";
     *  等级1,留"NMI_Handler";
     */
    #ifndef _BootCortexMx_IRQLV
        #define _BootCortexMx_IRQLV     (0)
    #endif

/************************************************ 我是分割线 ************************************************/
//=== 宏定义|枚举变量 =====================================|
    //中断相关配置
    #if(_BootCortexMx_IRQLV == 0)
        #define _BootCortexMx_IRQEN()       __set_PRIMASK(0)        //开中断
        #define _BootCortexMx_IRQDI()       __set_PRIMASK(1)        //关中断,留"NMI_Handler"和"HardFault_Handler";
    #elif(_BootCortexMx_IRQLV == 1)
        #define _BootCortexMx_IRQEN()       __set_FAULTMASK(0)      //开中断
        #define _BootCortexMx_IRQDI()       __set_FAULTMASK(1)      //关中断,留"NMI_Handler";
    #endif

    //CortexMx基本参数(支持M0,M3,M4)
        //地址
        #define _CortexMx_StartAddr_Flash   /*Mx Flash首地址(包含)*/        (0x00000000)
        #define _CortexMx_EndAddr_Flash     /*Mx Flash结束地址(包含)*/      (0x1FFFFFFF)
        #define _CortexMx_StartAddr_SRAM    /*Mx SRAM首地址(包含)*/         (0x20000000)
        #define _CortexMx_EndAddr_SRAM      /*Mx SRAM结束地址(包含)*/       (0x3FFFFFFF)
        //向量表(单位是4字节,外部向量表每个核心都不同,见单独配置)
        #define _CortexMx_IVT_FaultLen      /*Mx异常向量表长度(固定)*/      (16)        //1个MSP初始值加15个内核中断
        //MSP相关
        #define _CortexMx_Mask_MSP          /*MSP掩码,最低2位必定是0*/      (0x00000003)

    //CortexM0参数
        //地址
        #define _CortexM0_StartAddr_Flash   /*M0 Flash首地址(包含)*/        _CortexMx_StartAddr_Flash
        #define _CortexM0_EndAddr_Flash     /*M0 Flash结束地址(包含)*/      _CortexMx_EndAddr_Flash
        #define _CortexM0_StartAddr_SRAM    /*M0 SRAM首地址(包含)*/         _CortexMx_StartAddr_SRAM
        #define _CortexM0_EndAddr_SRAM      /*M0 SRAM结束地址(包含)*/       _CortexMx_EndAddr_SRAM
        //向量表(单位是4字节)
        #define _CortexM0_IVT_FaultLen      /*M0异常向量表长度(固定)*/      _CortexMx_IVT_FaultLen
        #define _CortexM0_IVT_IRQLen        /*M0外部中断向量表长度(固定)*/  (32)
        #define _CortexM0_IVT_Len           /*M0向量表长度(固定)*/          (_CortexM0_IVT_FaultLen+_CortexM0_IVT_IRQLen)
    //CortexM3参数
        //地址
        #define _CortexM3_StartAddr_Flash   /*M3 Flash首地址(包含)*/        _CortexMx_StartAddr_Flash
        #define _CortexM3_EndAddr_Flash     /*M3 Flash结束地址(包含)*/      _CortexMx_EndAddr_Flash
        #define _CortexM3_StartAddr_SRAM    /*M3 SRAM首地址(包含)*/         _CortexMx_StartAddr_SRAM
        #define _CortexM3_EndAddr_SRAM      /*M3 SRAM结束地址(包含)*/       _CortexMx_EndAddr_SRAM
        //向量表(单位是4字节)
        #define _CortexM3_IVT_FaultLen      /*M3异常向量表长度(固定)*/      _CortexMx_IVT_FaultLen
        #define _CortexM3_IVT_IRQLen        /*M3外部中断向量表长度(固定)*/  (240)
        #define _CortexM3_IVT_Len           /*M3向量表长度(固定)*/          (_CortexM3_IVT_FaultLen+_CortexM3_IVT_IRQLen)
    //CortexM4参数
        //地址
        #define _CortexM4_StartAddr_Flash   /*M4 Flash首地址(包含)*/        _CortexMx_StartAddr_Flash
        #define _CortexM4_EndAddr_Flash     /*M4 Flash结束地址(包含)*/      _CortexMx_EndAddr_Flash
        #define _CortexM4_StartAddr_SRAM    /*M4 SRAM首地址(包含)*/         _CortexMx_StartAddr_SRAM
        #define _CortexM4_EndAddr_SRAM      /*M4 SRAM结束地址(包含)*/       _CortexMx_EndAddr_SRAM
        //向量表(单位是4字节)
        #define _CortexM4_IVT_FaultLen      /*M4异常向量表长度(固定)*/      _CortexMx_IVT_FaultLen
        #define _CortexM4_IVT_IRQLen        /*M4外部中断向量表长度(固定)*/  (240)
        #define _CortexM4_IVT_Len           /*M4向量表长度(固定)*/          (_CortexM4_IVT_FaultLen+_CortexM4_IVT_IRQLen)

/************************************************ 我是分割线 ************************************************/
//=== 数据类型 ============================================|
    typedef void (*TypeBoot_fIRQ)(void);        //函数类型

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/*===中断向量表
 *=CortexMx内核向量表(16个,"[]"中为优先级)
 *      [00]__initial_sp            //[-3]MSP初始值
 *      [01]Reset_Handler           //[-2]复位
 *      [02]NMI_Handler             //[-1]NMI不可屏蔽中断
 *      [03]HardFault_Handler       //[可编程]硬件故障
 *      [04]MemManage_Handler       //[可编程]存储器管理错误(M0未使用)
 *      [05]BusFault_Handler        //[可编程]总线错误(M0未使用)
 *      [06]UsageFault_Handler      //[可编程]使用错误(M0未使用)
 *      [07]                        //[NA]未使用
 *      [08]                        //[NA]未使用
 *      [09]                        //[NA]未使用
 *      [10]                        //[NA]未使用
 *      [11]SVC_Handler             //[可编程]请求管理调用
 *      [12]DebugMon_Handler        //[可编程]调试监控(M0未使用)
 *      [13]                        //[NA]未使用
 *      [14]PendSV_Handler          //[可编程]可挂起系统调用
 *      [15]SysTick_Handler         //[可编程]滴答定时器中断
 */

/************************************************|
 * 描述:    判断MSP是否合法
 * 函数名:  Boot_CortexMx_MSPLegal
 * 参数[I]: volatile uint32_t MSP       //MSP的地址
 * 返回:    int     //0:合法;1:不合法;
 *** 说明:
 ***    主栈地址,4字节对齐,最低2位一定是0;
 ***    一定在内存中;
 *** 例程:  无
 ************************************************/
__STATIC_INLINE int Boot_CortexMx_MSPLegal(volatile uint32_t MSP)
{
    if( !(((MSP & _CortexMx_Mask_MSP) == 0) && (MSP>=_CortexMx_StartAddr_SRAM) && (MSP<=_CortexMx_EndAddr_SRAM)) ){
        //MSP地址不合法
        return(1);
    }
    return(0);
}

/************************************************|
 * 描述:    直接跳转地址
 * 函数名:  Boot_CortexMx_DirectGotoAPP
 * 参数[I]: volatile uint32_t *pAppAddr     //MSP的地址
 * 返回:    void
 *** 说明:  不判断直接跳转
 *** 例程:  无
 ************************************************/
__STATIC_INLINE void Boot_CortexMx_DirectGotoAPP(volatile uint32_t *pAppAddr)
{
//=== 跳转
    __set_MSP( (*(volatile uint32_t*)pAppAddr) );   //设置MSP
    (*(void(*)(void))pAppAddr[1])();                //跳转的到APP(第二个字是程序开始地址[复位地址])
}

/************************************************|
 * 描述:    跳转用户代码
 * 函数名:  Boot_CortexMx_GotoAPP
 * 参数[I]: volatile uint32_t *pAppAddr     //APP地址
 * 返回:    int     //1:失败
 *** 说明:
 ***    调用这个函数将会直接跳转到设定地址的APP程序;
 ***    若是此函数有返回值说明跳转失败;
 ***    注意:调用次函数会关闭全局中断,需要在APP程序中打开中断;
 *** 例程:  无
 ************************************************/
__STATIC_INLINE int Boot_CortexMx_GotoAPP(volatile uint32_t *pAppAddr)
{
    uint32_t MSP;
//===
    MSP = (*(volatile uint32_t*)pAppAddr);
    //判断MSP是否合法
    if(Boot_CortexMx_MSPLegal(MSP) != 0){
        //MSP地址错误
        return(1);
    }
    //关中断
    _BootCortexMx_IRQDI();
//=== 跳转
    __set_MSP(MSP);                     //设置MSP
    (*(void(*)(void))pAppAddr[1])();    //跳转的到APP(第二个字是程序开始地址[复位地址])
//===
    return(0);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 函数宏定义 ==========================================|
    //CortexMx跳转
    #define Boot_CortexM0_GotoAPP(_AppAddr) /*CortexM0跳转*/    Boot_CortexMx_GotoAPP((volatile uint32_t *)(_AppAddr))
    #define Boot_CortexM3_GotoAPP(_AppAddr) /*CortexM3跳转*/    Boot_CortexMx_GotoAPP((volatile uint32_t *)(_AppAddr))
    #define Boot_CortexM4_GotoAPP(_AppAddr) /*CortexM4跳转*/    Boot_CortexMx_GotoAPP((volatile uint32_t *)(_AppAddr))

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 文件结束
#endif
