/*=========================================================|
 | 文件名:  BootCortexM0_nRF51x.c
 | 描述:    BootCortexM0_nRF51x启动处理
 | 版本:    V0.01
 | 日期:    2021/07/29
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail: libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.CortexM0 nRF51x启动处理
 |  2.BootLoader中使用需要配置"_BootLoader_EN"等于1
 |  3.IVT(InterruptVectorTable)
 |  4.nRF51x没有直接指定在RAM里运行的寄存器,所以要用回调的方式来处理,
 |      使用初始192字节的RAM存放重新定义的向量表,中断时跳到指定的RAM地址;
 +-----------------------------------------------|
 +--- 版本说明
 |  V0.01:-2021/07/29
 |      1.初始化
 *========================================================*/
//=== 头文件
    #include "nrf.h"
    #include "BootCortexM0_nRF51x.h"
    #include "BootCortexMx.h"

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 私有类型 ============================================|
//=== 私有数据配置宏 ======================================|
//=== 私有宏定义 ==========================================|
    #define _nRF51x_StartAddr_SRAM      /*nRF51x SRAM起始地址*/     _CortexM0_StartAddr_SRAM
    #define _nRF51x_StartAddr_Flash     /*nRF51x Flash起始地址*/    _CortexM0_StartAddr_Flash
    #define _nRF51x_StartAddr_APP       /*nRF51x APP启动地址*/      _BootLoader_APPAddr

//=== 私有函数宏定义 ======================================|
//=== 私有全局变量 ========================================|
    /*=M0向量表
     *  内存占据48字,192字节;
     *  所以配置:
     *  内存起始地址:0x200000C0
     *  内存大小:    0x3F40
     */
    __attribute__((at(_nRF51x_StartAddr_SRAM))) static volatile TypeBoot_fIRQ s_IVT[_CortexM0_IVT_Len] = {0};   //中断向量表


//=== 私有函数 ============================================|

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/*===中断向量表
 *  M0中断向量表不可重映射,这边用软件来处理映射;
 *=CortexM0向量表(16个)
 *      [00]__initial_sp            //MSP初始值
 *      [01]Reset_Handler           //复位
 *      [02]NMI_Handler             //NMI不可屏蔽中断
 *      [03]HardFault_Handler       //硬件故障
 *      [04]                        //未使用
 *      [05]                        //未使用
 *      [06]                        //未使用
 *      [07]                        //未使用
 *      [08]                        //未使用
 *      [09]                        //未使用
 *      [10]                        //未使用
 *      [11]SVC_Handler             //请求管理调用
 *      [12]                        //未使用
 *      [13]                        //未使用
 *      [14]PendSV_Handler          //可挂起系统调用
 *      [15]SysTick_Handler         //滴答定时器中断
 *=外部中断,芯片厂商决定(32个)
 *      [16]POWER_CLOCK_IRQHandler
 *      [17]RADIO_IRQHandler
 *      [18]UART0_IRQHandler
 *      [19]SPI0_TWI0_IRQHandler
 *      [20]SPI1_TWI1_IRQHandler
 *      [21]                        //未使用
 *      [22]GPIOTE_IRQHandler
 *      [23]ADC_IRQHandler
 *      [24]TIMER0_IRQHandler
 *      [25]TIMER1_IRQHandler
 *      [26]TIMER2_IRQHandler
 *      [27]RTC0_IRQHandler
 *      [28]TEMP_IRQHandler
 *      [29]RNG_IRQHandler
 *      [30]ECB_IRQHandler
 *      [31]CCM_AAR_IRQHandler
 *      [32]WDT_IRQHandler
 *      [33]RTC1_IRQHandler
 *      [34]QDEC_IRQHandler
 *      [35]LPCOMP_IRQHandler
 *      [36]SWI0_IRQHandler
 *      [37]SWI1_IRQHandler
 *      [38]SWI2_IRQHandler
 *      [39]SWI3_IRQHandler
 *      [40]SWI4_IRQHandler
 *      [41]SWI5_IRQHandler
 *      [42]                        //未使用
 *      [43]                        //未使用
 *      [44]                        //未使用
 *      [45]                        //未使用
 *      [46]                        //未使用
 *      [47]                        //未使用
 */
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
/************************************************|
 * 描述:    跳转到应用程序地址
 * 版本:    0.01
 * 日期:    2021/07/29
 * 函数名:  Boot_nRF51x_GotoAPP
 * 参数[I]: uint AppAddr      //APP地址
 * 返回:    int     //1:失败
 *** 说明:
 ***    调用这个函数将会直接跳转到设定地址的APP程序;
 ***    若是此函数有返回值说明跳转失败;
 *** 例程:  无
 ************************************************/
int Boot_nRF51x_GotoAPP(uint AppAddr)
{
//===
    //判断MSP是否合法
    if(Boot_CortexMx_MSPLegal((*(volatile uint*)AppAddr)) != 0){
        //MSP地址错误
        return(1);
    }
    //关中断
    _BootCortexMx_IRQDI();
    //向量表重新映射
    memcpy((void*)s_IVT, (void*)AppAddr, _CortexM0_IVT_Len*4);  //从Flash读取向量表
    //跳转
    Boot_CortexMx_DirectGotoAPP((volatile uint*)AppAddr);       //直接跳转
    return(0);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
#if(_BootLoader_EN == 0)
/*=== 以下函数在"APP"中调用*/
/************************************************|
 * 描述:    APP程序启动
 * 版本:    0.01
 * 日期:    2021/07/30
 * 函数名:  Boot_nRF51x_APPStart
 * 参数[I]: void
 * 返回:    void
 *** 说明:
 ***    此函数必须在系统启动后立刻调用;
 ***    在调用前不可有任何中断操作,否则会死机;
 *** 例程:  无
 ************************************************/
void Boot_nRF51x_APPStart(void)
{
    //关中断
    _BootCortexMx_IRQDI();
    //向量表重新映射
     memcpy((void*)s_IVT, (void*)_nRF51x_StartAddr_APP, _CortexM0_IVT_Len*4);   //从Flash读取向量表
    //开中断
    _BootCortexMx_IRQEN();
}

/************************************************|
 * 描述:    "SystemInit"补丁函数
 * 版本:    0.01
 * 日期:    2021/07/31
 * 函数名:  $Sub$$SystemInit
 * 参数[N]: void
 * 返回:    void
 *** 说明:
 ***    只适用于MDK;
 ***    在MCU启动后给"SystemInit"增加补丁;
 ***    此处用于"BootLoader"静默启动;
 *** 例程:  无
 ************************************************/
#if( (_BootLoader_BootSilentStartEN == 1) && (defined(__CC_ARM) || defined(__CLANG_ARM)) )
void $Sub$$SystemInit(void)
{
    //外部函数
    extern int $Super$$SystemInit(void);
    //运行Boot向量表映射
    Boot_nRF51x_APPStart();
    //运行"SystemInit"
    $Super$$SystemInit();
}
#endif

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
#elif(_BootLoader_EN == 1)
/*=== 以下函数在"BootLoader"中调用*/
//=== 外部函数 ============================================|
    //中断入口,弱函数
    extern void NMI_Handler_(void);
    extern void HardFault_Handler_(void);
    extern void SVC_Handler_(void);
    extern void PendSV_Handler_(void);
    extern void SysTick_Handler_(void);
    extern void POWER_CLOCK_IRQHandler_(void);
    extern void RADIO_IRQHandler_(void);
    extern void UART0_IRQHandler_(void);
    extern void SPI0_TWI0_IRQHandler_(void);
    extern void SPI1_TWI1_IRQHandler_(void);
    extern void GPIOTE_IRQHandler_(void);
    extern void ADC_IRQHandler_(void);
    extern void TIMER0_IRQHandler_(void);
    extern void TIMER1_IRQHandler_(void);
    extern void TIMER2_IRQHandler_(void);
    extern void RTC0_IRQHandler_(void);
    extern void TEMP_IRQHandler_(void);
    extern void RNG_IRQHandler_(void);
    extern void ECB_IRQHandler_(void);
    extern void CCM_AAR_IRQHandler_(void);
    extern void WDT_IRQHandler_(void);
    extern void RTC1_IRQHandler_(void);
    extern void QDEC_IRQHandler_(void);
    extern void LPCOMP_IRQHandler_(void);
    extern void SWI0_IRQHandler_(void);
    extern void SWI1_IRQHandler_(void);
    extern void SWI2_IRQHandler_(void);
    extern void SWI3_IRQHandler_(void);
    extern void SWI4_IRQHandler_(void);
    extern void SWI5_IRQHandler_(void);

/************************************************|
 * 描述:    Boot程序启动
 * 版本:    0.01
 * 日期:    2021/07/29
 * 函数名:  Boot_nRF51x_BootStart
 * 参数[N]: void
 * 返回:    void
 *** 说明:
 ***    此函数必须在系统启动后立刻调用;
 ***    在调用前不可有任何中断操作,否则会死机;
 *** 例程:  无
 ************************************************/
void Boot_nRF51x_BootStart(void)
{
    //关中断
    _BootCortexMx_IRQDI();

//===更新中断向量表
    //M0
    s_IVT[02] = NMI_Handler_;           //NMI不可屏蔽中断
    s_IVT[03] = HardFault_Handler_;     //硬件故障
    s_IVT[11] = SVC_Handler_;           //请求管理调用
    s_IVT[14] = PendSV_Handler_;        //可挂起系统调用
    s_IVT[15] = SysTick_Handler_;       //滴答定时器中断
    //外部中断
    s_IVT[16] = POWER_CLOCK_IRQHandler_;
    s_IVT[17] = RADIO_IRQHandler_;
    s_IVT[18] = UART0_IRQHandler_;
    s_IVT[19] = SPI0_TWI0_IRQHandler_;
    s_IVT[20] = SPI1_TWI1_IRQHandler_;
    s_IVT[22] = GPIOTE_IRQHandler_;
    s_IVT[23] = ADC_IRQHandler_;
    s_IVT[24] = TIMER0_IRQHandler_;
    s_IVT[25] = TIMER1_IRQHandler_;
    s_IVT[26] = TIMER2_IRQHandler_;
    s_IVT[27] = RTC0_IRQHandler_;
    s_IVT[28] = TEMP_IRQHandler_;
    s_IVT[29] = RNG_IRQHandler_;
    s_IVT[30] = ECB_IRQHandler_;
    s_IVT[31] = CCM_AAR_IRQHandler_;
    s_IVT[32] = WDT_IRQHandler_;
    s_IVT[33] = RTC1_IRQHandler_;
    s_IVT[34] = QDEC_IRQHandler_;
    s_IVT[35] = LPCOMP_IRQHandler_;
    s_IVT[36] = SWI0_IRQHandler_;
    s_IVT[37] = SWI1_IRQHandler_;
    s_IVT[38] = SWI2_IRQHandler_;
    s_IVT[39] = SWI3_IRQHandler_;
    s_IVT[40] = SWI4_IRQHandler_;
    s_IVT[41] = SWI5_IRQHandler_;

    //开中断
    _BootCortexMx_IRQEN();
}

/************************************************|
 * 描述:    "SystemInit"补丁函数
 * 版本:    0.01
 * 日期:    2021/07/31
 * 函数名:  $Sub$$SystemInit
 * 参数[N]: void
 * 返回:    void
 *** 说明:
 ***    只适用于MDK;
 ***    在MCU启动后给"SystemInit"增加补丁;
 ***    此处用于"BootLoader"静默启动;
 *** 例程:  无
 ************************************************/
#if( (_BootLoader_BootSilentStartEN == 1) && (defined(__CC_ARM) || defined(__CLANG_ARM)) )
void $Sub$$SystemInit(void)
{
    //外部函数
    extern int $Super$$SystemInit(void);
    //运行Boot向量表映射
    Boot_nRF51x_BootStart();
    //运行"SystemInit"
    $Super$$SystemInit();
}
#endif

/************************************************ 我是分割线 ************************************************/
/*=== 向量表重载*/
/*CortexM0向量表(16个)*/
/*ID:[00]*/         //MSP初始值
/*ID:[01]*/         //复位
/*ID:[02]*/ void NMI_Handler(void){             (*(void(*)(void))s_IVT[02])(); }    //NMI不可屏蔽中断
/*ID:[03]*/ void HardFault_Handler(void){       (*(void(*)(void))s_IVT[03])(); }    //硬件故障
/*ID:[04]*/         //未使用
/*ID:[05]*/         //未使用
/*ID:[06]*/         //未使用
/*ID:[07]*/         //未使用
/*ID:[08]*/         //未使用
/*ID:[09]*/         //未使用
/*ID:[10]*/         //未使用
/*ID:[11]*/ void SVC_Handler(void){             (*(void(*)(void))s_IVT[11])(); }    //请求管理调用
/*ID:[12]*/         //未使用
/*ID:[13]*/         //未使用
/*ID:[14]*/ void PendSV_Handler(void){          (*(void(*)(void))s_IVT[14])(); }    //可挂起系统调用
/*ID:[15]*/ void SysTick_Handler(void){         (*(void(*)(void))s_IVT[15])(); }    //滴答定时器中断
/*外部中断,芯片厂商决定(32个)*/
/*ID:[16]*/ void POWER_CLOCK_IRQHandler(void){  (*(void(*)(void))s_IVT[16])(); }
/*ID:[17]*/ void RADIO_IRQHandler(void){        (*(void(*)(void))s_IVT[17])(); }
/*ID:[18]*/ void UART0_IRQHandler(void){        (*(void(*)(void))s_IVT[18])(); }
/*ID:[19]*/ void SPI0_TWI0_IRQHandler(void){    (*(void(*)(void))s_IVT[19])(); }
/*ID:[20]*/ void SPI1_TWI1_IRQHandler(void){    (*(void(*)(void))s_IVT[20])(); }
/*ID:[21]*/         //未使用
/*ID:[22]*/ void GPIOTE_IRQHandler(void){       (*(void(*)(void))s_IVT[22])(); }
/*ID:[23]*/ void ADC_IRQHandler(void){          (*(void(*)(void))s_IVT[23])(); }
/*ID:[24]*/ void TIMER0_IRQHandler(void){       (*(void(*)(void))s_IVT[24])(); }
/*ID:[25]*/ void TIMER1_IRQHandler(void){       (*(void(*)(void))s_IVT[25])(); }
/*ID:[26]*/ void TIMER2_IRQHandler(void){       (*(void(*)(void))s_IVT[26])(); }
/*ID:[27]*/ void RTC0_IRQHandler(void){         (*(void(*)(void))s_IVT[27])(); }
/*ID:[28]*/ void TEMP_IRQHandler(void){         (*(void(*)(void))s_IVT[28])(); }
/*ID:[29]*/ void RNG_IRQHandler(void){          (*(void(*)(void))s_IVT[29])(); }
/*ID:[30]*/ void ECB_IRQHandler(void){          (*(void(*)(void))s_IVT[30])(); }
/*ID:[31]*/ void CCM_AAR_IRQHandler(void){      (*(void(*)(void))s_IVT[31])(); }
/*ID:[32]*/ void WDT_IRQHandler(void){          (*(void(*)(void))s_IVT[32])(); }
/*ID:[33]*/ void RTC1_IRQHandler(void){         (*(void(*)(void))s_IVT[33])(); }
/*ID:[34]*/ void QDEC_IRQHandler(void){         (*(void(*)(void))s_IVT[34])(); }
/*ID:[35]*/ void LPCOMP_IRQHandler(void){       (*(void(*)(void))s_IVT[35])(); }
/*ID:[36]*/ void SWI0_IRQHandler(void){         (*(void(*)(void))s_IVT[36])(); }
/*ID:[37]*/ void SWI1_IRQHandler(void){         (*(void(*)(void))s_IVT[37])(); }
/*ID:[38]*/ void SWI2_IRQHandler(void){         (*(void(*)(void))s_IVT[38])(); }
/*ID:[39]*/ void SWI3_IRQHandler(void){         (*(void(*)(void))s_IVT[39])(); }
/*ID:[40]*/ void SWI4_IRQHandler(void){         (*(void(*)(void))s_IVT[40])(); }
/*ID:[41]*/ void SWI5_IRQHandler(void){         (*(void(*)(void))s_IVT[41])(); }
/*ID:[42]*/         //未使用
/*ID:[43]*/         //未使用
/*ID:[44]*/         //未使用
/*ID:[45]*/         //未使用
/*ID:[46]*/         //未使用
/*ID:[47]*/         //未使用
/************************************************ 我是分割线 ************************************************/
/*=== 向量表重定义*/
/*CortexM0向量表(16个)*/
/*ID:[00]*/         //MSP初始值
/*ID:[01]*/         //复位
/*ID:[02]*/ __WEAK void NMI_Handler_(void){}            //NMI不可屏蔽中断
/*ID:[03]*/ __WEAK void HardFault_Handler_(void){}      //硬件故障
/*ID:[04]*/         //未使用
/*ID:[05]*/         //未使用
/*ID:[06]*/         //未使用
/*ID:[07]*/         //未使用
/*ID:[08]*/         //未使用
/*ID:[09]*/         //未使用
/*ID:[10]*/         //未使用
/*ID:[11]*/ __WEAK void SVC_Handler_(void){}            //请求管理调用
/*ID:[12]*/         //未使用
/*ID:[13]*/         //未使用
/*ID:[14]*/ __WEAK void PendSV_Handler_(void){}         //可挂起系统调用
/*ID:[15]*/ __WEAK void SysTick_Handler_(void){}        //滴答定时器中断
/*外部中断,芯片厂商决定(32个)*/
/*ID:[16]*/ __WEAK void POWER_CLOCK_IRQHandler_(void){}
/*ID:[17]*/ __WEAK void RADIO_IRQHandler_(void){}
/*ID:[18]*/ __WEAK void UART0_IRQHandler_(void){}
/*ID:[19]*/ __WEAK void SPI0_TWI0_IRQHandler_(void){}
/*ID:[20]*/ __WEAK void SPI1_TWI1_IRQHandler_(void){}
/*ID:[21]*/         //未使用
/*ID:[22]*/ __WEAK void GPIOTE_IRQHandler_(void){}
/*ID:[23]*/ __WEAK void ADC_IRQHandler_(void){}
/*ID:[24]*/ __WEAK void TIMER0_IRQHandler_(void){}
/*ID:[25]*/ __WEAK void TIMER1_IRQHandler_(void){}
/*ID:[26]*/ __WEAK void TIMER2_IRQHandler_(void){}
/*ID:[27]*/ __WEAK void RTC0_IRQHandler_(void){}
/*ID:[28]*/ __WEAK void TEMP_IRQHandler_(void){}
/*ID:[29]*/ __WEAK void RNG_IRQHandler_(void){}
/*ID:[30]*/ __WEAK void ECB_IRQHandler_(void){}
/*ID:[31]*/ __WEAK void CCM_AAR_IRQHandler_(void){}
/*ID:[32]*/ __WEAK void WDT_IRQHandler_(void){}
/*ID:[33]*/ __WEAK void RTC1_IRQHandler_(void){}
/*ID:[34]*/ __WEAK void QDEC_IRQHandler_(void){}
/*ID:[35]*/ __WEAK void LPCOMP_IRQHandler_(void){}
/*ID:[36]*/ __WEAK void SWI0_IRQHandler_(void){}
/*ID:[37]*/ __WEAK void SWI1_IRQHandler_(void){}
/*ID:[38]*/ __WEAK void SWI2_IRQHandler_(void){}
/*ID:[39]*/ __WEAK void SWI3_IRQHandler_(void){}
/*ID:[40]*/ __WEAK void SWI4_IRQHandler_(void){}
/*ID:[41]*/ __WEAK void SWI5_IRQHandler_(void){}
/*ID:[42]*/         //未使用
/*ID:[43]*/         //未使用
/*ID:[44]*/         //未使用
/*ID:[45]*/         //未使用
/*ID:[46]*/         //未使用
/*ID:[47]*/         //未使用
//===
#endif
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
