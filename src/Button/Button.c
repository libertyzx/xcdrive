/*=========================================================|
 | 文件名:  Button.c
 | 描述:    按键控制
 | 版本:    V0.1
 | 日期:    2023/01/05
 | 语言:    C语言
 | 作者:    libertyzx
 | E-mail:  libertyzx@163.com
 +-----------------------------------------------|
 | 开源协议: MIT License
 +-----------------------------------------------|
 +--- 说明
 |  1.按键延时及状态说明:
 |      按键处理有3种状态:
 |      1).按下
 |          按键按下时间超过设定的"去抖动时间(ph->DebounceTime)",才会判定按键按下;
 |          注意,"去抖动时间"可以为0,按键直接触发;
 |      2).长按连续触发
 |          按键确定有效按下后且一直按着,则会按"按下时间(ph->PressTime)"连续触发"Button_Weak_TriggerProcess"函数,
 |          按"ph->PressTime"时间周期触发,触发计数为"ph->PressCount";
 |      3).松开
 |          松开触发只有在有效按下后且松开按键才会被触发;
 +-----------------------------------------------|
 +--- 版本说明
 |  V1.01:-2023/01/05
 |      1.初始化
 *========================================================*/
//=== 头文件
    #include "Button.h"

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
//=== 私有类型 ===================================|
//=== 私有宏定义 =================================|
    //按键判断状态
    #define _Release    /*松开*/    (0)
    #define _Debounce   /*去抖动*/  (1)
    #define _Press      /*按下*/    (2)

//=== 私有宏定义配置 =============================|
//=== 私有函数宏 =================================|
//=== 私有全局变量 ===============================|
//=== 私有函数 ===================================|
    //弱函数
    extern int Button_Weak_GetValue(HandleButton *ph);                                  //获取按键值
    extern void Button_Weak_TriggerProcess(HandleButton *ph, TypeButton_State State);   //触发处理

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */

/************************************************|
 * 描述:    初始化
 * 函数名:  Button_Init
 * 参数[H]: HandleButton *ph        //句柄
 * 参数[I]: uchar ID                //句柄ID
 * 参数[I]: ushort DebounceTime     //去抖时间(ms)
 * 参数[I]: ushort PressTime        //按下时间(ms)
 * 返回:    void
 *** 说明:  输入处理初始化
 *** 例程:  无
 ************************************************/
void Button_Init(HandleButton *ph, uchar ID, ushort DebounceTime, ushort PressTime)
{
    //配置
    ph->ID = ID;
    ph->DebounceTime = DebounceTime;    //去抖动时间
    ph->PressTime = PressTime;          //按下时间
    //数据
    ph->PressCount = 0;
    ph->Time = 0;
}

/************************************************|
 * 描述:    按键处理
 * 函数名:  Button_Process
 * 参数[H]: HandleButton *ph        //句柄
 * 返回:    void
 *** 说明:  循环调用
 *** 例程:  无
 ************************************************/
void Button_Process(HandleButton *ph)
{
    if(Button_Weak_GetValue(ph) == 1){
        //按下
        if(ph->State == _Release){
            //是松开状态
            ph->PressCount = 0;
            ph->Time = _Time_GetTick(); //更新TICK
            ph->State = _Debounce;
        }
        //去抖动判断
        if(ph->State == _Debounce){
            //是去抖状态
            if(_Time_CompareTick(ph->Time, ph->DebounceTime)){
                //去抖动完成确定被按下
                ph->State = _Press;
                ph->PressCount++;
                ph->Time = _Time_GetTick(); //更新TICK
                Button_Weak_TriggerProcess(ph, _Button_S_Press);    //按下处理
            }
        }
        //一直按下处理
        if( (ph->State == _Press) && (ph->PressTime != 0) ){
            //是按下状态
            if(_Time_CompareTick(ph->Time, ph->PressTime)){
                ph->PressCount++;
                ph->Time = _Time_GetTick(); //更新TICK
                Button_Weak_TriggerProcess(ph, _Button_S_LongPress);    //一直按着
            }
        }
    }
    else{
        //松开
        if(ph->State == _Press){
            Button_Weak_TriggerProcess(ph, _Button_S_Release);  //松开处理
        }
        ph->State = _Release;
    }
}

/************************************************ 我是分割线 ************************************************/

/************************************************|
 * 描述:    设置去抖时间
 * 函数名:  Button_SetDebounceTime
 * 参数[H]: HandleButton *ph        //句柄
 * 参数[I]: ushort DebounceTime     //去抖时间(ms)
 * 返回:    void
 *** 说明:  无
 *** 例程:  无
 ************************************************/
void Button_SetDebounceTime(HandleButton *ph, ushort ms)
{
    ph->DebounceTime = ms;
}

/************************************************|
 * 描述:    获取去抖时间
 * 函数名:  Button_SetDebounceTime
 * 参数[H]: HandleButton *ph    //句柄
 * 返回:    int                 //返回去抖动时间
 *** 说明:  无
 *** 例程:  无
 ************************************************/
int Button_GetDebounceTime(HandleButton *ph)
{
    return(ph->DebounceTime);
}

/************************************************|
 * 描述:    设置按下时间
 * 函数名:  Button_SetDebounceTime
 * 参数[H]: Handle Button *ph   //句柄
 * 参数[I]: ushort PressTime    //按下时间(ms)
 * 返回:    void
 *** 说明:  无
 *** 例程:  无
 ************************************************/
void Button_SetPressTime(HandleButton *ph, ushort ms)
{
    ph->PressTime = ms;
}

/************************************************|
 * 描述:    获取按下时间
 * 函数名:  Button_SetDebounceTime
 * 参数[H]: Handle Button *ph   //句柄
 * 返回:    int
 *** 说明:  无
 *** 例程:  无
 ************************************************/
int Button_GetPressTime(HandleButton *ph)
{
    return(ph->PressTime);
}

/************************************************|
 * 描述:    获取按下次数
 * 函数名:  Button_GetPressCount
 * 参数[H]: Handle Button *ph   //句柄
 * 返回:    int
 *** 说明:  无
 *** 例程:  无
 ************************************************/
int Button_GetPressCount(HandleButton *ph)
{
    return(ph->PressCount);
}

/************************************************|
 * 描述:    获取ID
 * 函数名:  Button_GetID
 * 参数[H]: Handle Button *ph   //句柄
 * 返回:    int
 *** 说明:  无
 *** 例程:  无
 ************************************************/
int Button_GetID(HandleButton *ph)
{
    return(ph->ID);
}

/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
#if(_Button_WeakFunc_EN == 1)
/*=== 弱函数实现-说明*/
/**/

/************************************************|
 * 描述:    [弱函数]获取按键值
 * 函数名:  Button_Weak_GetPinValue
 * 参数[H]: HandleButton *ph
 * 返回:    int     //1表示按下,0表示松开
 *** 说明:  获取值
 *** 例程:  无
 ************************************************/
__WEAK int Button_Weak_GetValue(HandleButton *ph)
{
    return(0);
}

/************************************************|
 * 描述:    [弱函数]触发处理
 * 函数名:  Button_Weak_TriggerProcess
 * 参数[H]: HandleButton *ph
 * 参数[I]: TypeButton_State State
 *  +=触发状态
 *  | _Button_S_Release     //松开
 *  | _Button_S_Press       //按下
 *  | _Button_S_LongPress   //长按下
 * 返回:    void
 *** 说明:  所有处理统一在此弱函数中处理
 *** 例程:  无
 ************************************************/
__WEAK void Button_Weak_TriggerProcess(HandleButton *ph, TypeButton_State State)
{}

//===
#endif

/************************************************ 我是分割线 ************************************************/
/*
 ************************************************************************************************************|
 ************************************************ 我是分割线 ************************************************|
 ************************************************************************************************************|
 */
