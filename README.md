# XCDrive
## 1.介绍
和硬件相关,实现各种驱动

## 2.说明
将每个需要的功能独立包含即可

## 3.每个模块说明

> "+"表示文件夹; ">"表示上个"+"文件夹下的文件(只是文件的名字,表示一个模块);

|目录|模块名|说明|
|:-|:-|:-|
|**PCF8563**|RTC
|**TSM12**|12位电容触摸

---
---
### 3.1.PCF8563
>依赖库
- XCBase->XCBase.h
- XCAPI->Time->Time.h
- XCAPI->Data->BCD.h

>配置宏

|配置参数|默认值|说明|
|:-|:-:|:-|
|_PCF8563_WeakFunc_EN|1|弱函数使能(1使能)

>弱函数

|弱函数名|说明|
|:-|:-:|
|PCF8563_WeakIO_ReadReg|读寄存器|
|PCF8563_WeakIO_WriteReg|写寄存器|

---

### 3.2.TSM12
>依赖库
- XCBase->XCBase.h

>配置宏

|配置参数|默认值|说明|
|:-|:-:|:-|
|_TSM12_WeakFunc_EN|1|弱函数使能(1使能)|

>弱函数

|弱函数名|说明|
|:-|:-|
|TSM12_WeakIO_RST|复位|
|TSM12_WeakIO_IIC|IIC使能|
|TSM12_WeakIO_ReadReg|读寄存器数据|
|TSM12_WeakIO_WriteReg|写寄存器数据|